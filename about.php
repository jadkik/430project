<?php
//Simply routes the page to the ABOUT page, which is an anchor, by kik
//view is a function that acts as a controller/router (MVC-like)

require_once __DIR__ . '/include.php';

view('static_about');

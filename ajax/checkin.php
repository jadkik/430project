<?php
//By KiK
require_once __DIR__ . '/../include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$today = date('Y-m-d') . ' 00:00:00';

$query = MemberAttendanceQuery::create()
    ->filterByMemberId($user->getId())
    ->filterByCourseId($_POST['course_id'])
    ->filterByCreatedAt(array('min' => $today))
    ->orderByCreatedAt('desc')
    ->limit(1);

$attendances = $query->find();

if (count($attendances) > 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You already checked into this course today.',
    ));
    die();
}

$course = CoursesQuery::create()->findPk($_POST['course_id']);

$today = date('Y-m-d');
$start_dt = new DateTime($today . ' ' . $course->getStartTime()->format('H:i:s'));
$end_dt = new DateTime($today . ' ' . $course->getEndTime()->format('H:i:s'));
$now_dt = new DateTime();

$start_diff = $now_dt->diff($start_dt);
$end_diff = $now_dt->diff($end_dt);

if ($start_diff->i < -5) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You cannot check in now. The course has not started yet.',
    ));
    die();
} else if ($end_diff->i > 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You cannot check in now. The course has already finished.',
    ));
    die();
}

$attendances = new MemberAttendance();
$attendances->setMemberId($user->getId());
$attendances->setCourseId($_POST['course_id']);
$attendances->setAction($_POST['action']);
$attendances->save();

echo json_encode(array(
    'success' => true,
    'message' => 'Checked in.',
));

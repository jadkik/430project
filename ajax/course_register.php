<?php
//By Kik
require_once __DIR__ . '/../include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$base_query = CourseMemberRegistrationQuery::create()
    ->filterByMemberId($user->getId())
    ->filterByCourseId($_POST['course_id']);

$registrations = $base_query->limit(1)->find();

if (count($registrations) > 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You are not registered to this course.',
    ));
    die();
}

$reg = new CourseMemberRegistration();
$reg->setMemberId($user->getId());
$reg->setCourseId($_POST['course_id']);
$reg->save();

$course = CoursesQuery::create()->findPk($_POST['course_id']);
$member = MembersQuery::create()->findPk($user->getId());

$today = new DateTime();
$end = $member->getPlanExpiry();

$sch = new Schedule();
$sch->setMemberId($user->getId());
$sch->setTitle($course->getName());
$sch->setStartTime($course->getStartTime()->format('H:i:s'));
$sch->setEndTime($course->getEndTime()->format('H:i:s'));
$sch->setWeekdays($course->getWeekdays());
$sch->setStartDate($today);
$sch->setEndDate($end);
$sch->save();

echo json_encode(array(
    'success' => true,
    'message' => 'Registered to course.',
));

<?php
//By Kik
require_once __DIR__ . '/../include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$base_query = CourseMemberRegistrationQuery::create()
    ->filterByMemberId($user->getId())
    ->filterByCourseId($_POST['course_id']);

$registrations = $base_query->limit(1)->find();

if (count($registrations) == 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You are not registered to this course.',
    ));
    die();
}

$base_query->delete();

MemberAttendanceQuery::create()
    ->filterByMemberId($user->getId())
    ->filterByCourseId($_POST['course_id'])
    ->delete();

$course = CoursesQuery::create()->findPk($_POST['course_id']);

$sch = ScheduleQuery::create()
    ->filterByMemberId($user->getId())
    ->filterByStartTime($course->getStartTime()->format('H:i:s'))
    ->filterByEndTime($course->getEndTime()->format('H:i:s'))
    ->filterByWeekdays($course->getWeekdays())
    ->delete();

echo json_encode(array(
    'success' => true,
    'message' => 'Unregistered from course.',
));

<?php
//By Nappa
require_once __DIR__ . '/../include.php';
 
/*
 * Example PHP implementation used for the index.html example
 */
 
// DataTables PHP library
require_once __DIR__ .  "/../Editor-PHP-1.5.5/php/DataTables.php" ;
 
// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
 
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'courses' )
    ->fields(
        //Field::inst( 'username' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'courses.id' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'courses.name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'courses.trainer_id' )->options( 'trainers', 'id', 'last_name' )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'courses.description' ),
        Field::inst( 'courses.start_time' ),
        Field::inst( 'courses.end_time' ),
        Field::inst( 'courses.is_deluxe' ),
		
		Field::inst( 'trainers.first_name' ),
		Field::inst( 'trainers.last_name' )

    )
	->leftJoin( 'trainers', 'trainers.id', '=', 'courses.trainer_id' )
    ->process( $_POST )
    ->json();

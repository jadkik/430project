<?php
//By Nappa

require_once __DIR__ . '/../include.php';
 
/*
 * Example PHP implementation used for the index.html example
 */
 
// DataTables PHP library
require_once __DIR__ .  "/../Editor-PHP-1.5.5/php/DataTables.php" ;
 
// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
 
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'facilities' )
    ->fields(
        //Field::inst( 'username' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'facilities.id' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'facilities.name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'facilities.capacity' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'facilities.description' ),
        Field::inst( 'facilities.opening_time' ),
        Field::inst( 'facilities.closing_time' ),
        Field::inst( 'facilities.is_deluxe' )

    )
    ->process( $_POST )
    ->json();

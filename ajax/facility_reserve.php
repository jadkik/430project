<?php
//By Kik
require_once __DIR__ . '/../include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$facility = FacilitiesQuery::create()->findPk($_POST['facility_id']);
$member = MembersQuery::create()->findPk($user->getId());

if (empty($facility)) {
    echo json_encode(array(
        'success' => false,
        'message' => 'No such facility.',
    ));
    die();
}

if ($member->hasPlanExpired()) {
    echo json_encode(array(
        'success' => false,
        'message' => 'Your curreny plan has expired. Please renew your plan.',
    ));
    die();
}

if ($facility->getIsDeluxe() && !$member->hasDeluxePlan()) {
    echo json_encode(array(
        'success' => false,
        'message' => 'This facility requires a deluxe plan. Please renew your plan.',
    ));
    die();
}

$from_time = new DateTime($_POST['from_time']);
$to_time = new DateTime($_POST['to_time']);

$from_dt = new DateTime($_POST['date'] . ' ' . $_POST['from_time']);
$to_dt = new DateTime($_POST['date'] . ' ' . $_POST['to_time']);

if ($from_dt >= $to_dt) {
    echo json_encode(array(
        'success' => false,
        'message' => 'Please select a valid date range.',
    ));
    die();
}

if (!($from_time > $facility->getOpeningTime() && $from_time < $facility->getClosingTime()
        && $to_time > $facility->getOpeningTime() && $to_time < $facility->getClosingTime())) {
    echo json_encode(array(
        'success' => false,
        'message' => 'The facility is not available at that time.',
    ));
    die();
}

$member_reservations = ReservationsQuery::create()
    ->filterByMemberId($user->getId())
    ->condition('start_cond', 'Reservations.StartTime < ?', $to_dt)
    ->condition('end_cond', 'Reservations.EndTime > ?', $from_dt)
    ->where(array('start_cond', 'end_cond'), 'or')
    ->find();

if (count($member_reservations) > 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'The chosen time overlaps with another of your reservations.',
    ));
    die();
}

$facility_reservations = ReservationsQuery::create()
    ->filterByFacilityId($facility->getId())
    ->condition('start_cond', 'Reservations.StartTime < ?', $to_dt)
    ->condition('end_cond', 'Reservations.EndTime > ?', $from_dt)
    ->where(array('start_cond', 'end_cond'), 'or')
    ->find();

if (count($facility_reservations) > $facility->getCapacity()) {
    echo json_encode(array(
        'success' => false,
        'message' => 'This facility is fully booked at that time.',
    ));
    die();
}

$reg = new Reservations();
$reg->setMemberId($user->getId());
$reg->setFacilityId($_POST['facility_id']);
$reg->setStartTime($from_dt);
$reg->setEndTime($to_dt);
$reg->save();

echo json_encode(array(
    'success' => true,
    'message' => 'Registered to course.',
));

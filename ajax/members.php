<?php
//By Nappa
require_once __DIR__ . '/../include.php';
 
/*
 * Example PHP implementation used for the index.html example
 */
 
// DataTables PHP library
require_once __DIR__ .  "/../Editor-PHP-1.5.5/php/DataTables.php" ;
 
// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
 
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'members' )
    ->fields(
        //Field::inst( 'username' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'members.id' )
			->options( 'users', 'id', 'id' )
            ->validator( 'Validate::dbValues' ),
		Field::inst( 'users.username' ),
        Field::inst( 'members.first_name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'members.last_name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'members.plan' ),
        Field::inst( 'members.height' ),
        Field::inst( 'members.weight' ),
        Field::inst( 'members.trainer_id' )
			->options( 'trainers', 'id', 'last_name' )
            ->validator( 'Validate::dbValues' ),
		Field::inst( 'trainers.first_name' ),
		Field::inst( 'trainers.last_name' ),

        Field::inst( 'members.plan_expiry' )
            ->validator( 'Validate::dateFormat', array(
                "empty"   => true,
                "format"  => Format::DATE_ISO_8601,
                "message" => "Please enter a date in the format yyyy-mm-dd"
            ) )
            ->getFormatter( 'Format::date_sql_to_format', Format::DATE_ISO_8601 )
            ->setFormatter( 'Format::date_format_to_sql', Format::DATE_ISO_8601 )
    )
	->leftJoin( 'users', 'users.id', '=', 'members.id' )
	->leftJoin( 'trainers', 'trainers.id', '=', 'members.trainer_id' )
    ->process( $_POST )
    ->json();
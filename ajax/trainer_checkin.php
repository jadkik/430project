<?php
//By KiK
require_once __DIR__ . '/../include.php';

$user = get_logged_in_user('trainer');
if (empty($user)) {
    redirect('login.php');
}

$action = $_POST['action'];
$today = date('Y-m-d') . ' 00:00:00';

$query = TrainerAttendanceQuery::create()
    ->filterByTrainerId($user->getId())
    ->filterByAction($action)
    ->filterByCreatedAt(array('min' => $today))
    ->orderByCreatedAt('desc')
    ->limit(1);

$attendances = $query->find();

if (count($attendances) > 0) {
    echo json_encode(array(
        'success' => false,
        'message' => 'You already checked ' . ($action == 'signin'? 'in' : 'out') . ' today.',
    ));
    die();
}

$attendances = new TrainerAttendance();
$attendances->setTrainerId($user->getId());
$attendances->setAction($action);
$attendances->save();

echo json_encode(array(
    'success' => true,
    'message' => ($action == 'signin'? 'Checked in.' : 'Checked out.'),
));

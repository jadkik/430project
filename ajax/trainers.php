<?php
//By Nappa
require_once __DIR__ . '/../include.php';
 
/*
 * Example PHP implementation used for the index.html example
 */
 
// DataTables PHP library
require_once __DIR__ .  "/../Editor-PHP-1.5.5/php/DataTables.php" ;
 
// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
 
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'trainers' )
    ->fields(
        Field::inst( 'trainers.id' )
            ->options( 'users', 'id', 'id' )
            ->validator( 'Validate::dbValues' )
            ->set(Field::SET_CREATE),
        Field::inst( 'users.id' )->set(Field::SET_NONE),
        Field::inst( 'users.username' )->set(Field::SET_NONE),
        //~ Field::inst( 'users.password' )->set(Field::SET_NONE),
        Field::inst( 'users.role' )->set(Field::SET_NONE),
        Field::inst( 'trainers.first_name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'trainers.last_name' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'trainers.hours_required' ),
        Field::inst( 'trainers.manager_id' )
            ->options( 'managers', 'id', 'last_name' )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'managers.first_name' ),
        Field::inst( 'managers.last_name' )
    )
    ->leftJoin( 'users', 'users.id', '=', 'trainers.id' )
    ->leftJoin( 'managers', 'managers.id', '=', 'trainers.manager_id' )
    ->on('preCreate', function($editor, $values) {
        //~ var_dump($values);
        // before create
        $user = new Users();
        $user->setUsername($values['users']['username']);
        $user->setPassword(password_hash($values['users']['password'], PASSWORD_DEFAULT));
        $user->setRole('trainer');
        $user->save();

        //$values['trainers']['id'] = $user->getId();
        $editor->field( 'trainers.id' )
               ->setValue( $user->getId() );
    })
    ->on('preEdit', function($editor, $id, $values) {
        //~ var_dump($values);
        // before create
        $user = (new UsersQuery())->findPk($id);
        //~ $user->setUsername($values['users']['username']);
        if (isset($values['users']) && isset($values['users']['password']) && !empty($values['users']['password'])) {
            $user->setPassword(password_hash($values['users']['password'], PASSWORD_DEFAULT));
        }
        //~ $user->setRole('trainer');
        $user->save();

        //$values['trainers']['id'] = $user->getId();
        //~ $editor->field( 'trainers.id' )
               //~ ->setValue( $user->getId() );
    })
    ->process( $_POST )
    ->json();

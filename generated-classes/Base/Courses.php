<?php

namespace Base;

use \CourseMemberRegistration as ChildCourseMemberRegistration;
use \CourseMemberRegistrationQuery as ChildCourseMemberRegistrationQuery;
use \Courses as ChildCourses;
use \CoursesQuery as ChildCoursesQuery;
use \MemberAttendance as ChildMemberAttendance;
use \MemberAttendanceQuery as ChildMemberAttendanceQuery;
use \Trainers as ChildTrainers;
use \TrainersQuery as ChildTrainersQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CourseMemberRegistrationTableMap;
use Map\CoursesTableMap;
use Map\MemberAttendanceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'courses' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Courses implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CoursesTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the trainer_id field.
     *
     * @var        int
     */
    protected $trainer_id;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the start_time field.
     *
     * @var        DateTime
     */
    protected $start_time;

    /**
     * The value for the end_time field.
     *
     * @var        DateTime
     */
    protected $end_time;

    /**
     * The value for the is_deluxe field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_deluxe;

    /**
     * The value for the weekdays field.
     *
     * @var        string
     */
    protected $weekdays;

    /**
     * @var        ChildTrainers
     */
    protected $aTrainers;

    /**
     * @var        ObjectCollection|ChildCourseMemberRegistration[] Collection to store aggregation of ChildCourseMemberRegistration objects.
     */
    protected $collCourseMemberRegistrations;
    protected $collCourseMemberRegistrationsPartial;

    /**
     * @var        ObjectCollection|ChildMemberAttendance[] Collection to store aggregation of ChildMemberAttendance objects.
     */
    protected $collMemberAttendances;
    protected $collMemberAttendancesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCourseMemberRegistration[]
     */
    protected $courseMemberRegistrationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMemberAttendance[]
     */
    protected $memberAttendancesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_deluxe = false;
    }

    /**
     * Initializes internal state of Base\Courses object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Courses</code> instance.  If
     * <code>obj</code> is an instance of <code>Courses</code>, delegates to
     * <code>equals(Courses)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Courses The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [trainer_id] column value.
     *
     * @return int
     */
    public function getTrainerId()
    {
        return $this->trainer_id;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [optionally formatted] temporal [start_time] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartTime($format = NULL)
    {
        if ($format === null) {
            return $this->start_time;
        } else {
            return $this->start_time instanceof \DateTimeInterface ? $this->start_time->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [end_time] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndTime($format = NULL)
    {
        if ($format === null) {
            return $this->end_time;
        } else {
            return $this->end_time instanceof \DateTimeInterface ? $this->end_time->format($format) : null;
        }
    }

    /**
     * Get the [is_deluxe] column value.
     *
     * @return boolean
     */
    public function getIsDeluxe()
    {
        return $this->is_deluxe;
    }

    /**
     * Get the [is_deluxe] column value.
     *
     * @return boolean
     */
    public function isDeluxe()
    {
        return $this->getIsDeluxe();
    }

    /**
     * Get the [weekdays] column value.
     *
     * @return string
     */
    public function getWeekdays()
    {
        return $this->weekdays;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CoursesTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[CoursesTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [trainer_id] column.
     *
     * @param int $v new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setTrainerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->trainer_id !== $v) {
            $this->trainer_id = $v;
            $this->modifiedColumns[CoursesTableMap::COL_TRAINER_ID] = true;
        }

        if ($this->aTrainers !== null && $this->aTrainers->getId() !== $v) {
            $this->aTrainers = null;
        }

        return $this;
    } // setTrainerId()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[CoursesTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Sets the value of [start_time] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setStartTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->start_time !== null || $dt !== null) {
            if ($this->start_time === null || $dt === null || $dt->format("H:i:s") !== $this->start_time->format("H:i:s")) {
                $this->start_time = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CoursesTableMap::COL_START_TIME] = true;
            }
        } // if either are not null

        return $this;
    } // setStartTime()

    /**
     * Sets the value of [end_time] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setEndTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->end_time !== null || $dt !== null) {
            if ($this->end_time === null || $dt === null || $dt->format("H:i:s") !== $this->end_time->format("H:i:s")) {
                $this->end_time = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CoursesTableMap::COL_END_TIME] = true;
            }
        } // if either are not null

        return $this;
    } // setEndTime()

    /**
     * Sets the value of the [is_deluxe] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setIsDeluxe($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deluxe !== $v) {
            $this->is_deluxe = $v;
            $this->modifiedColumns[CoursesTableMap::COL_IS_DELUXE] = true;
        }

        return $this;
    } // setIsDeluxe()

    /**
     * Set the value of [weekdays] column.
     *
     * @param string $v new value
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function setWeekdays($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->weekdays !== $v) {
            $this->weekdays = $v;
            $this->modifiedColumns[CoursesTableMap::COL_WEEKDAYS] = true;
        }

        return $this;
    } // setWeekdays()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_deluxe !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CoursesTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CoursesTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CoursesTableMap::translateFieldName('TrainerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trainer_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CoursesTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CoursesTableMap::translateFieldName('StartTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->start_time = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CoursesTableMap::translateFieldName('EndTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->end_time = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CoursesTableMap::translateFieldName('IsDeluxe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deluxe = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CoursesTableMap::translateFieldName('Weekdays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weekdays = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = CoursesTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Courses'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aTrainers !== null && $this->trainer_id !== $this->aTrainers->getId()) {
            $this->aTrainers = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CoursesTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCoursesQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aTrainers = null;
            $this->collCourseMemberRegistrations = null;

            $this->collMemberAttendances = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Courses::setDeleted()
     * @see Courses::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCoursesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CoursesTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTrainers !== null) {
                if ($this->aTrainers->isModified() || $this->aTrainers->isNew()) {
                    $affectedRows += $this->aTrainers->save($con);
                }
                $this->setTrainers($this->aTrainers);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->courseMemberRegistrationsScheduledForDeletion !== null) {
                if (!$this->courseMemberRegistrationsScheduledForDeletion->isEmpty()) {
                    \CourseMemberRegistrationQuery::create()
                        ->filterByPrimaryKeys($this->courseMemberRegistrationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->courseMemberRegistrationsScheduledForDeletion = null;
                }
            }

            if ($this->collCourseMemberRegistrations !== null) {
                foreach ($this->collCourseMemberRegistrations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->memberAttendancesScheduledForDeletion !== null) {
                if (!$this->memberAttendancesScheduledForDeletion->isEmpty()) {
                    \MemberAttendanceQuery::create()
                        ->filterByPrimaryKeys($this->memberAttendancesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->memberAttendancesScheduledForDeletion = null;
                }
            }

            if ($this->collMemberAttendances !== null) {
                foreach ($this->collMemberAttendances as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CoursesTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CoursesTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CoursesTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_TRAINER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'trainer_id';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_START_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'start_time';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_END_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'end_time';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_IS_DELUXE)) {
            $modifiedColumns[':p' . $index++]  = 'is_deluxe';
        }
        if ($this->isColumnModified(CoursesTableMap::COL_WEEKDAYS)) {
            $modifiedColumns[':p' . $index++]  = 'weekdays';
        }

        $sql = sprintf(
            'INSERT INTO courses (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'trainer_id':
                        $stmt->bindValue($identifier, $this->trainer_id, PDO::PARAM_INT);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'start_time':
                        $stmt->bindValue($identifier, $this->start_time ? $this->start_time->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'end_time':
                        $stmt->bindValue($identifier, $this->end_time ? $this->end_time->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'is_deluxe':
                        $stmt->bindValue($identifier, (int) $this->is_deluxe, PDO::PARAM_INT);
                        break;
                    case 'weekdays':
                        $stmt->bindValue($identifier, $this->weekdays, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CoursesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getTrainerId();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getStartTime();
                break;
            case 5:
                return $this->getEndTime();
                break;
            case 6:
                return $this->getIsDeluxe();
                break;
            case 7:
                return $this->getWeekdays();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Courses'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Courses'][$this->hashCode()] = true;
        $keys = CoursesTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getTrainerId(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getStartTime(),
            $keys[5] => $this->getEndTime(),
            $keys[6] => $this->getIsDeluxe(),
            $keys[7] => $this->getWeekdays(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aTrainers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'trainers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'trainers';
                        break;
                    default:
                        $key = 'Trainers';
                }

                $result[$key] = $this->aTrainers->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCourseMemberRegistrations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'courseMemberRegistrations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'course_member_registrations';
                        break;
                    default:
                        $key = 'CourseMemberRegistrations';
                }

                $result[$key] = $this->collCourseMemberRegistrations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMemberAttendances) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'memberAttendances';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'member_attendances';
                        break;
                    default:
                        $key = 'MemberAttendances';
                }

                $result[$key] = $this->collMemberAttendances->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Courses
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CoursesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Courses
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setTrainerId($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setStartTime($value);
                break;
            case 5:
                $this->setEndTime($value);
                break;
            case 6:
                $this->setIsDeluxe($value);
                break;
            case 7:
                $this->setWeekdays($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CoursesTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setTrainerId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setStartTime($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setEndTime($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsDeluxe($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setWeekdays($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Courses The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CoursesTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CoursesTableMap::COL_ID)) {
            $criteria->add(CoursesTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_NAME)) {
            $criteria->add(CoursesTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_TRAINER_ID)) {
            $criteria->add(CoursesTableMap::COL_TRAINER_ID, $this->trainer_id);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_DESCRIPTION)) {
            $criteria->add(CoursesTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_START_TIME)) {
            $criteria->add(CoursesTableMap::COL_START_TIME, $this->start_time);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_END_TIME)) {
            $criteria->add(CoursesTableMap::COL_END_TIME, $this->end_time);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_IS_DELUXE)) {
            $criteria->add(CoursesTableMap::COL_IS_DELUXE, $this->is_deluxe);
        }
        if ($this->isColumnModified(CoursesTableMap::COL_WEEKDAYS)) {
            $criteria->add(CoursesTableMap::COL_WEEKDAYS, $this->weekdays);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCoursesQuery::create();
        $criteria->add(CoursesTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Courses (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setTrainerId($this->getTrainerId());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setStartTime($this->getStartTime());
        $copyObj->setEndTime($this->getEndTime());
        $copyObj->setIsDeluxe($this->getIsDeluxe());
        $copyObj->setWeekdays($this->getWeekdays());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCourseMemberRegistrations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCourseMemberRegistration($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMemberAttendances() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMemberAttendance($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Courses Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildTrainers object.
     *
     * @param  ChildTrainers $v
     * @return $this|\Courses The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTrainers(ChildTrainers $v = null)
    {
        if ($v === null) {
            $this->setTrainerId(NULL);
        } else {
            $this->setTrainerId($v->getId());
        }

        $this->aTrainers = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTrainers object, it will not be re-added.
        if ($v !== null) {
            $v->addCourses($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTrainers object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTrainers The associated ChildTrainers object.
     * @throws PropelException
     */
    public function getTrainers(ConnectionInterface $con = null)
    {
        if ($this->aTrainers === null && ($this->trainer_id !== null)) {
            $this->aTrainers = ChildTrainersQuery::create()->findPk($this->trainer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTrainers->addCoursess($this);
             */
        }

        return $this->aTrainers;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CourseMemberRegistration' == $relationName) {
            return $this->initCourseMemberRegistrations();
        }
        if ('MemberAttendance' == $relationName) {
            return $this->initMemberAttendances();
        }
    }

    /**
     * Clears out the collCourseMemberRegistrations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCourseMemberRegistrations()
     */
    public function clearCourseMemberRegistrations()
    {
        $this->collCourseMemberRegistrations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCourseMemberRegistrations collection loaded partially.
     */
    public function resetPartialCourseMemberRegistrations($v = true)
    {
        $this->collCourseMemberRegistrationsPartial = $v;
    }

    /**
     * Initializes the collCourseMemberRegistrations collection.
     *
     * By default this just sets the collCourseMemberRegistrations collection to an empty array (like clearcollCourseMemberRegistrations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCourseMemberRegistrations($overrideExisting = true)
    {
        if (null !== $this->collCourseMemberRegistrations && !$overrideExisting) {
            return;
        }

        $collectionClassName = CourseMemberRegistrationTableMap::getTableMap()->getCollectionClassName();

        $this->collCourseMemberRegistrations = new $collectionClassName;
        $this->collCourseMemberRegistrations->setModel('\CourseMemberRegistration');
    }

    /**
     * Gets an array of ChildCourseMemberRegistration objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCourses is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCourseMemberRegistration[] List of ChildCourseMemberRegistration objects
     * @throws PropelException
     */
    public function getCourseMemberRegistrations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCourseMemberRegistrationsPartial && !$this->isNew();
        if (null === $this->collCourseMemberRegistrations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCourseMemberRegistrations) {
                // return empty collection
                $this->initCourseMemberRegistrations();
            } else {
                $collCourseMemberRegistrations = ChildCourseMemberRegistrationQuery::create(null, $criteria)
                    ->filterByCourses($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCourseMemberRegistrationsPartial && count($collCourseMemberRegistrations)) {
                        $this->initCourseMemberRegistrations(false);

                        foreach ($collCourseMemberRegistrations as $obj) {
                            if (false == $this->collCourseMemberRegistrations->contains($obj)) {
                                $this->collCourseMemberRegistrations->append($obj);
                            }
                        }

                        $this->collCourseMemberRegistrationsPartial = true;
                    }

                    return $collCourseMemberRegistrations;
                }

                if ($partial && $this->collCourseMemberRegistrations) {
                    foreach ($this->collCourseMemberRegistrations as $obj) {
                        if ($obj->isNew()) {
                            $collCourseMemberRegistrations[] = $obj;
                        }
                    }
                }

                $this->collCourseMemberRegistrations = $collCourseMemberRegistrations;
                $this->collCourseMemberRegistrationsPartial = false;
            }
        }

        return $this->collCourseMemberRegistrations;
    }

    /**
     * Sets a collection of ChildCourseMemberRegistration objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $courseMemberRegistrations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCourses The current object (for fluent API support)
     */
    public function setCourseMemberRegistrations(Collection $courseMemberRegistrations, ConnectionInterface $con = null)
    {
        /** @var ChildCourseMemberRegistration[] $courseMemberRegistrationsToDelete */
        $courseMemberRegistrationsToDelete = $this->getCourseMemberRegistrations(new Criteria(), $con)->diff($courseMemberRegistrations);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->courseMemberRegistrationsScheduledForDeletion = clone $courseMemberRegistrationsToDelete;

        foreach ($courseMemberRegistrationsToDelete as $courseMemberRegistrationRemoved) {
            $courseMemberRegistrationRemoved->setCourses(null);
        }

        $this->collCourseMemberRegistrations = null;
        foreach ($courseMemberRegistrations as $courseMemberRegistration) {
            $this->addCourseMemberRegistration($courseMemberRegistration);
        }

        $this->collCourseMemberRegistrations = $courseMemberRegistrations;
        $this->collCourseMemberRegistrationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CourseMemberRegistration objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CourseMemberRegistration objects.
     * @throws PropelException
     */
    public function countCourseMemberRegistrations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCourseMemberRegistrationsPartial && !$this->isNew();
        if (null === $this->collCourseMemberRegistrations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCourseMemberRegistrations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCourseMemberRegistrations());
            }

            $query = ChildCourseMemberRegistrationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCourses($this)
                ->count($con);
        }

        return count($this->collCourseMemberRegistrations);
    }

    /**
     * Method called to associate a ChildCourseMemberRegistration object to this object
     * through the ChildCourseMemberRegistration foreign key attribute.
     *
     * @param  ChildCourseMemberRegistration $l ChildCourseMemberRegistration
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function addCourseMemberRegistration(ChildCourseMemberRegistration $l)
    {
        if ($this->collCourseMemberRegistrations === null) {
            $this->initCourseMemberRegistrations();
            $this->collCourseMemberRegistrationsPartial = true;
        }

        if (!$this->collCourseMemberRegistrations->contains($l)) {
            $this->doAddCourseMemberRegistration($l);

            if ($this->courseMemberRegistrationsScheduledForDeletion and $this->courseMemberRegistrationsScheduledForDeletion->contains($l)) {
                $this->courseMemberRegistrationsScheduledForDeletion->remove($this->courseMemberRegistrationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCourseMemberRegistration $courseMemberRegistration The ChildCourseMemberRegistration object to add.
     */
    protected function doAddCourseMemberRegistration(ChildCourseMemberRegistration $courseMemberRegistration)
    {
        $this->collCourseMemberRegistrations[]= $courseMemberRegistration;
        $courseMemberRegistration->setCourses($this);
    }

    /**
     * @param  ChildCourseMemberRegistration $courseMemberRegistration The ChildCourseMemberRegistration object to remove.
     * @return $this|ChildCourses The current object (for fluent API support)
     */
    public function removeCourseMemberRegistration(ChildCourseMemberRegistration $courseMemberRegistration)
    {
        if ($this->getCourseMemberRegistrations()->contains($courseMemberRegistration)) {
            $pos = $this->collCourseMemberRegistrations->search($courseMemberRegistration);
            $this->collCourseMemberRegistrations->remove($pos);
            if (null === $this->courseMemberRegistrationsScheduledForDeletion) {
                $this->courseMemberRegistrationsScheduledForDeletion = clone $this->collCourseMemberRegistrations;
                $this->courseMemberRegistrationsScheduledForDeletion->clear();
            }
            $this->courseMemberRegistrationsScheduledForDeletion[]= clone $courseMemberRegistration;
            $courseMemberRegistration->setCourses(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Courses is new, it will return
     * an empty collection; or if this Courses has previously
     * been saved, it will retrieve related CourseMemberRegistrations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Courses.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCourseMemberRegistration[] List of ChildCourseMemberRegistration objects
     */
    public function getCourseMemberRegistrationsJoinMembers(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCourseMemberRegistrationQuery::create(null, $criteria);
        $query->joinWith('Members', $joinBehavior);

        return $this->getCourseMemberRegistrations($query, $con);
    }

    /**
     * Clears out the collMemberAttendances collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMemberAttendances()
     */
    public function clearMemberAttendances()
    {
        $this->collMemberAttendances = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMemberAttendances collection loaded partially.
     */
    public function resetPartialMemberAttendances($v = true)
    {
        $this->collMemberAttendancesPartial = $v;
    }

    /**
     * Initializes the collMemberAttendances collection.
     *
     * By default this just sets the collMemberAttendances collection to an empty array (like clearcollMemberAttendances());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMemberAttendances($overrideExisting = true)
    {
        if (null !== $this->collMemberAttendances && !$overrideExisting) {
            return;
        }

        $collectionClassName = MemberAttendanceTableMap::getTableMap()->getCollectionClassName();

        $this->collMemberAttendances = new $collectionClassName;
        $this->collMemberAttendances->setModel('\MemberAttendance');
    }

    /**
     * Gets an array of ChildMemberAttendance objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCourses is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMemberAttendance[] List of ChildMemberAttendance objects
     * @throws PropelException
     */
    public function getMemberAttendances(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMemberAttendancesPartial && !$this->isNew();
        if (null === $this->collMemberAttendances || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMemberAttendances) {
                // return empty collection
                $this->initMemberAttendances();
            } else {
                $collMemberAttendances = ChildMemberAttendanceQuery::create(null, $criteria)
                    ->filterByCourses($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMemberAttendancesPartial && count($collMemberAttendances)) {
                        $this->initMemberAttendances(false);

                        foreach ($collMemberAttendances as $obj) {
                            if (false == $this->collMemberAttendances->contains($obj)) {
                                $this->collMemberAttendances->append($obj);
                            }
                        }

                        $this->collMemberAttendancesPartial = true;
                    }

                    return $collMemberAttendances;
                }

                if ($partial && $this->collMemberAttendances) {
                    foreach ($this->collMemberAttendances as $obj) {
                        if ($obj->isNew()) {
                            $collMemberAttendances[] = $obj;
                        }
                    }
                }

                $this->collMemberAttendances = $collMemberAttendances;
                $this->collMemberAttendancesPartial = false;
            }
        }

        return $this->collMemberAttendances;
    }

    /**
     * Sets a collection of ChildMemberAttendance objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $memberAttendances A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCourses The current object (for fluent API support)
     */
    public function setMemberAttendances(Collection $memberAttendances, ConnectionInterface $con = null)
    {
        /** @var ChildMemberAttendance[] $memberAttendancesToDelete */
        $memberAttendancesToDelete = $this->getMemberAttendances(new Criteria(), $con)->diff($memberAttendances);


        $this->memberAttendancesScheduledForDeletion = $memberAttendancesToDelete;

        foreach ($memberAttendancesToDelete as $memberAttendanceRemoved) {
            $memberAttendanceRemoved->setCourses(null);
        }

        $this->collMemberAttendances = null;
        foreach ($memberAttendances as $memberAttendance) {
            $this->addMemberAttendance($memberAttendance);
        }

        $this->collMemberAttendances = $memberAttendances;
        $this->collMemberAttendancesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MemberAttendance objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MemberAttendance objects.
     * @throws PropelException
     */
    public function countMemberAttendances(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMemberAttendancesPartial && !$this->isNew();
        if (null === $this->collMemberAttendances || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMemberAttendances) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMemberAttendances());
            }

            $query = ChildMemberAttendanceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCourses($this)
                ->count($con);
        }

        return count($this->collMemberAttendances);
    }

    /**
     * Method called to associate a ChildMemberAttendance object to this object
     * through the ChildMemberAttendance foreign key attribute.
     *
     * @param  ChildMemberAttendance $l ChildMemberAttendance
     * @return $this|\Courses The current object (for fluent API support)
     */
    public function addMemberAttendance(ChildMemberAttendance $l)
    {
        if ($this->collMemberAttendances === null) {
            $this->initMemberAttendances();
            $this->collMemberAttendancesPartial = true;
        }

        if (!$this->collMemberAttendances->contains($l)) {
            $this->doAddMemberAttendance($l);

            if ($this->memberAttendancesScheduledForDeletion and $this->memberAttendancesScheduledForDeletion->contains($l)) {
                $this->memberAttendancesScheduledForDeletion->remove($this->memberAttendancesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMemberAttendance $memberAttendance The ChildMemberAttendance object to add.
     */
    protected function doAddMemberAttendance(ChildMemberAttendance $memberAttendance)
    {
        $this->collMemberAttendances[]= $memberAttendance;
        $memberAttendance->setCourses($this);
    }

    /**
     * @param  ChildMemberAttendance $memberAttendance The ChildMemberAttendance object to remove.
     * @return $this|ChildCourses The current object (for fluent API support)
     */
    public function removeMemberAttendance(ChildMemberAttendance $memberAttendance)
    {
        if ($this->getMemberAttendances()->contains($memberAttendance)) {
            $pos = $this->collMemberAttendances->search($memberAttendance);
            $this->collMemberAttendances->remove($pos);
            if (null === $this->memberAttendancesScheduledForDeletion) {
                $this->memberAttendancesScheduledForDeletion = clone $this->collMemberAttendances;
                $this->memberAttendancesScheduledForDeletion->clear();
            }
            $this->memberAttendancesScheduledForDeletion[]= clone $memberAttendance;
            $memberAttendance->setCourses(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Courses is new, it will return
     * an empty collection; or if this Courses has previously
     * been saved, it will retrieve related MemberAttendances from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Courses.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMemberAttendance[] List of ChildMemberAttendance objects
     */
    public function getMemberAttendancesJoinMembers(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMemberAttendanceQuery::create(null, $criteria);
        $query->joinWith('Members', $joinBehavior);

        return $this->getMemberAttendances($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aTrainers) {
            $this->aTrainers->removeCourses($this);
        }
        $this->id = null;
        $this->name = null;
        $this->trainer_id = null;
        $this->description = null;
        $this->start_time = null;
        $this->end_time = null;
        $this->is_deluxe = null;
        $this->weekdays = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCourseMemberRegistrations) {
                foreach ($this->collCourseMemberRegistrations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMemberAttendances) {
                foreach ($this->collMemberAttendances as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCourseMemberRegistrations = null;
        $this->collMemberAttendances = null;
        $this->aTrainers = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CoursesTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace Base;

use \Courses as ChildCourses;
use \CoursesQuery as ChildCoursesQuery;
use \Exception;
use \PDO;
use Map\CoursesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'courses' table.
 *
 *
 *
 * @method     ChildCoursesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCoursesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCoursesQuery orderByTrainerId($order = Criteria::ASC) Order by the trainer_id column
 * @method     ChildCoursesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildCoursesQuery orderByStartTime($order = Criteria::ASC) Order by the start_time column
 * @method     ChildCoursesQuery orderByEndTime($order = Criteria::ASC) Order by the end_time column
 * @method     ChildCoursesQuery orderByIsDeluxe($order = Criteria::ASC) Order by the is_deluxe column
 * @method     ChildCoursesQuery orderByWeekdays($order = Criteria::ASC) Order by the weekdays column
 *
 * @method     ChildCoursesQuery groupById() Group by the id column
 * @method     ChildCoursesQuery groupByName() Group by the name column
 * @method     ChildCoursesQuery groupByTrainerId() Group by the trainer_id column
 * @method     ChildCoursesQuery groupByDescription() Group by the description column
 * @method     ChildCoursesQuery groupByStartTime() Group by the start_time column
 * @method     ChildCoursesQuery groupByEndTime() Group by the end_time column
 * @method     ChildCoursesQuery groupByIsDeluxe() Group by the is_deluxe column
 * @method     ChildCoursesQuery groupByWeekdays() Group by the weekdays column
 *
 * @method     ChildCoursesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCoursesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCoursesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCoursesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCoursesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCoursesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCoursesQuery leftJoinTrainers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trainers relation
 * @method     ChildCoursesQuery rightJoinTrainers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trainers relation
 * @method     ChildCoursesQuery innerJoinTrainers($relationAlias = null) Adds a INNER JOIN clause to the query using the Trainers relation
 *
 * @method     ChildCoursesQuery joinWithTrainers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Trainers relation
 *
 * @method     ChildCoursesQuery leftJoinWithTrainers() Adds a LEFT JOIN clause and with to the query using the Trainers relation
 * @method     ChildCoursesQuery rightJoinWithTrainers() Adds a RIGHT JOIN clause and with to the query using the Trainers relation
 * @method     ChildCoursesQuery innerJoinWithTrainers() Adds a INNER JOIN clause and with to the query using the Trainers relation
 *
 * @method     ChildCoursesQuery leftJoinCourseMemberRegistration($relationAlias = null) Adds a LEFT JOIN clause to the query using the CourseMemberRegistration relation
 * @method     ChildCoursesQuery rightJoinCourseMemberRegistration($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CourseMemberRegistration relation
 * @method     ChildCoursesQuery innerJoinCourseMemberRegistration($relationAlias = null) Adds a INNER JOIN clause to the query using the CourseMemberRegistration relation
 *
 * @method     ChildCoursesQuery joinWithCourseMemberRegistration($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CourseMemberRegistration relation
 *
 * @method     ChildCoursesQuery leftJoinWithCourseMemberRegistration() Adds a LEFT JOIN clause and with to the query using the CourseMemberRegistration relation
 * @method     ChildCoursesQuery rightJoinWithCourseMemberRegistration() Adds a RIGHT JOIN clause and with to the query using the CourseMemberRegistration relation
 * @method     ChildCoursesQuery innerJoinWithCourseMemberRegistration() Adds a INNER JOIN clause and with to the query using the CourseMemberRegistration relation
 *
 * @method     ChildCoursesQuery leftJoinMemberAttendance($relationAlias = null) Adds a LEFT JOIN clause to the query using the MemberAttendance relation
 * @method     ChildCoursesQuery rightJoinMemberAttendance($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MemberAttendance relation
 * @method     ChildCoursesQuery innerJoinMemberAttendance($relationAlias = null) Adds a INNER JOIN clause to the query using the MemberAttendance relation
 *
 * @method     ChildCoursesQuery joinWithMemberAttendance($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MemberAttendance relation
 *
 * @method     ChildCoursesQuery leftJoinWithMemberAttendance() Adds a LEFT JOIN clause and with to the query using the MemberAttendance relation
 * @method     ChildCoursesQuery rightJoinWithMemberAttendance() Adds a RIGHT JOIN clause and with to the query using the MemberAttendance relation
 * @method     ChildCoursesQuery innerJoinWithMemberAttendance() Adds a INNER JOIN clause and with to the query using the MemberAttendance relation
 *
 * @method     \TrainersQuery|\CourseMemberRegistrationQuery|\MemberAttendanceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCourses findOne(ConnectionInterface $con = null) Return the first ChildCourses matching the query
 * @method     ChildCourses findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCourses matching the query, or a new ChildCourses object populated from the query conditions when no match is found
 *
 * @method     ChildCourses findOneById(int $id) Return the first ChildCourses filtered by the id column
 * @method     ChildCourses findOneByName(string $name) Return the first ChildCourses filtered by the name column
 * @method     ChildCourses findOneByTrainerId(int $trainer_id) Return the first ChildCourses filtered by the trainer_id column
 * @method     ChildCourses findOneByDescription(string $description) Return the first ChildCourses filtered by the description column
 * @method     ChildCourses findOneByStartTime(string $start_time) Return the first ChildCourses filtered by the start_time column
 * @method     ChildCourses findOneByEndTime(string $end_time) Return the first ChildCourses filtered by the end_time column
 * @method     ChildCourses findOneByIsDeluxe(boolean $is_deluxe) Return the first ChildCourses filtered by the is_deluxe column
 * @method     ChildCourses findOneByWeekdays(string $weekdays) Return the first ChildCourses filtered by the weekdays column *

 * @method     ChildCourses requirePk($key, ConnectionInterface $con = null) Return the ChildCourses by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOne(ConnectionInterface $con = null) Return the first ChildCourses matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourses requireOneById(int $id) Return the first ChildCourses filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByName(string $name) Return the first ChildCourses filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByTrainerId(int $trainer_id) Return the first ChildCourses filtered by the trainer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByDescription(string $description) Return the first ChildCourses filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByStartTime(string $start_time) Return the first ChildCourses filtered by the start_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByEndTime(string $end_time) Return the first ChildCourses filtered by the end_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByIsDeluxe(boolean $is_deluxe) Return the first ChildCourses filtered by the is_deluxe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCourses requireOneByWeekdays(string $weekdays) Return the first ChildCourses filtered by the weekdays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCourses[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCourses objects based on current ModelCriteria
 * @method     ChildCourses[]|ObjectCollection findById(int $id) Return ChildCourses objects filtered by the id column
 * @method     ChildCourses[]|ObjectCollection findByName(string $name) Return ChildCourses objects filtered by the name column
 * @method     ChildCourses[]|ObjectCollection findByTrainerId(int $trainer_id) Return ChildCourses objects filtered by the trainer_id column
 * @method     ChildCourses[]|ObjectCollection findByDescription(string $description) Return ChildCourses objects filtered by the description column
 * @method     ChildCourses[]|ObjectCollection findByStartTime(string $start_time) Return ChildCourses objects filtered by the start_time column
 * @method     ChildCourses[]|ObjectCollection findByEndTime(string $end_time) Return ChildCourses objects filtered by the end_time column
 * @method     ChildCourses[]|ObjectCollection findByIsDeluxe(boolean $is_deluxe) Return ChildCourses objects filtered by the is_deluxe column
 * @method     ChildCourses[]|ObjectCollection findByWeekdays(string $weekdays) Return ChildCourses objects filtered by the weekdays column
 * @method     ChildCourses[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CoursesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CoursesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Courses', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCoursesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCoursesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCoursesQuery) {
            return $criteria;
        }
        $query = new ChildCoursesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCourses|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CoursesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CoursesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCourses A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, trainer_id, description, start_time, end_time, is_deluxe, weekdays FROM courses WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCourses $obj */
            $obj = new ChildCourses();
            $obj->hydrate($row);
            CoursesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCourses|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CoursesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CoursesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CoursesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CoursesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the trainer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTrainerId(1234); // WHERE trainer_id = 1234
     * $query->filterByTrainerId(array(12, 34)); // WHERE trainer_id IN (12, 34)
     * $query->filterByTrainerId(array('min' => 12)); // WHERE trainer_id > 12
     * </code>
     *
     * @see       filterByTrainers()
     *
     * @param     mixed $trainerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByTrainerId($trainerId = null, $comparison = null)
    {
        if (is_array($trainerId)) {
            $useMinMax = false;
            if (isset($trainerId['min'])) {
                $this->addUsingAlias(CoursesTableMap::COL_TRAINER_ID, $trainerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trainerId['max'])) {
                $this->addUsingAlias(CoursesTableMap::COL_TRAINER_ID, $trainerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_TRAINER_ID, $trainerId, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the start_time column
     *
     * Example usage:
     * <code>
     * $query->filterByStartTime('2011-03-14'); // WHERE start_time = '2011-03-14'
     * $query->filterByStartTime('now'); // WHERE start_time = '2011-03-14'
     * $query->filterByStartTime(array('max' => 'yesterday')); // WHERE start_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $startTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByStartTime($startTime = null, $comparison = null)
    {
        if (is_array($startTime)) {
            $useMinMax = false;
            if (isset($startTime['min'])) {
                $this->addUsingAlias(CoursesTableMap::COL_START_TIME, $startTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startTime['max'])) {
                $this->addUsingAlias(CoursesTableMap::COL_START_TIME, $startTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_START_TIME, $startTime, $comparison);
    }

    /**
     * Filter the query on the end_time column
     *
     * Example usage:
     * <code>
     * $query->filterByEndTime('2011-03-14'); // WHERE end_time = '2011-03-14'
     * $query->filterByEndTime('now'); // WHERE end_time = '2011-03-14'
     * $query->filterByEndTime(array('max' => 'yesterday')); // WHERE end_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $endTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByEndTime($endTime = null, $comparison = null)
    {
        if (is_array($endTime)) {
            $useMinMax = false;
            if (isset($endTime['min'])) {
                $this->addUsingAlias(CoursesTableMap::COL_END_TIME, $endTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endTime['max'])) {
                $this->addUsingAlias(CoursesTableMap::COL_END_TIME, $endTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_END_TIME, $endTime, $comparison);
    }

    /**
     * Filter the query on the is_deluxe column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeluxe(true); // WHERE is_deluxe = true
     * $query->filterByIsDeluxe('yes'); // WHERE is_deluxe = true
     * </code>
     *
     * @param     boolean|string $isDeluxe The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByIsDeluxe($isDeluxe = null, $comparison = null)
    {
        if (is_string($isDeluxe)) {
            $isDeluxe = in_array(strtolower($isDeluxe), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CoursesTableMap::COL_IS_DELUXE, $isDeluxe, $comparison);
    }

    /**
     * Filter the query on the weekdays column
     *
     * Example usage:
     * <code>
     * $query->filterByWeekdays('fooValue');   // WHERE weekdays = 'fooValue'
     * $query->filterByWeekdays('%fooValue%'); // WHERE weekdays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $weekdays The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByWeekdays($weekdays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($weekdays)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $weekdays)) {
                $weekdays = str_replace('*', '%', $weekdays);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CoursesTableMap::COL_WEEKDAYS, $weekdays, $comparison);
    }

    /**
     * Filter the query by a related \Trainers object
     *
     * @param \Trainers|ObjectCollection $trainers The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByTrainers($trainers, $comparison = null)
    {
        if ($trainers instanceof \Trainers) {
            return $this
                ->addUsingAlias(CoursesTableMap::COL_TRAINER_ID, $trainers->getId(), $comparison);
        } elseif ($trainers instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CoursesTableMap::COL_TRAINER_ID, $trainers->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTrainers() only accepts arguments of type \Trainers or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Trainers relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function joinTrainers($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Trainers');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Trainers');
        }

        return $this;
    }

    /**
     * Use the Trainers relation Trainers object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TrainersQuery A secondary query class using the current class as primary query
     */
    public function useTrainersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTrainers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Trainers', '\TrainersQuery');
    }

    /**
     * Filter the query by a related \CourseMemberRegistration object
     *
     * @param \CourseMemberRegistration|ObjectCollection $courseMemberRegistration the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByCourseMemberRegistration($courseMemberRegistration, $comparison = null)
    {
        if ($courseMemberRegistration instanceof \CourseMemberRegistration) {
            return $this
                ->addUsingAlias(CoursesTableMap::COL_ID, $courseMemberRegistration->getCourseId(), $comparison);
        } elseif ($courseMemberRegistration instanceof ObjectCollection) {
            return $this
                ->useCourseMemberRegistrationQuery()
                ->filterByPrimaryKeys($courseMemberRegistration->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourseMemberRegistration() only accepts arguments of type \CourseMemberRegistration or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CourseMemberRegistration relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function joinCourseMemberRegistration($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CourseMemberRegistration');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CourseMemberRegistration');
        }

        return $this;
    }

    /**
     * Use the CourseMemberRegistration relation CourseMemberRegistration object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourseMemberRegistrationQuery A secondary query class using the current class as primary query
     */
    public function useCourseMemberRegistrationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourseMemberRegistration($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CourseMemberRegistration', '\CourseMemberRegistrationQuery');
    }

    /**
     * Filter the query by a related \MemberAttendance object
     *
     * @param \MemberAttendance|ObjectCollection $memberAttendance the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCoursesQuery The current query, for fluid interface
     */
    public function filterByMemberAttendance($memberAttendance, $comparison = null)
    {
        if ($memberAttendance instanceof \MemberAttendance) {
            return $this
                ->addUsingAlias(CoursesTableMap::COL_ID, $memberAttendance->getCourseId(), $comparison);
        } elseif ($memberAttendance instanceof ObjectCollection) {
            return $this
                ->useMemberAttendanceQuery()
                ->filterByPrimaryKeys($memberAttendance->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMemberAttendance() only accepts arguments of type \MemberAttendance or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MemberAttendance relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function joinMemberAttendance($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MemberAttendance');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MemberAttendance');
        }

        return $this;
    }

    /**
     * Use the MemberAttendance relation MemberAttendance object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MemberAttendanceQuery A secondary query class using the current class as primary query
     */
    public function useMemberAttendanceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMemberAttendance($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MemberAttendance', '\MemberAttendanceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCourses $courses Object to remove from the list of results
     *
     * @return $this|ChildCoursesQuery The current query, for fluid interface
     */
    public function prune($courses = null)
    {
        if ($courses) {
            $this->addUsingAlias(CoursesTableMap::COL_ID, $courses->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the courses table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CoursesTableMap::clearInstancePool();
            CoursesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CoursesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CoursesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CoursesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CoursesQuery

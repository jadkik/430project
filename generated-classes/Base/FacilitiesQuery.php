<?php

namespace Base;

use \Facilities as ChildFacilities;
use \FacilitiesQuery as ChildFacilitiesQuery;
use \Exception;
use \PDO;
use Map\FacilitiesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'facilities' table.
 *
 *
 *
 * @method     ChildFacilitiesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFacilitiesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildFacilitiesQuery orderByCapacity($order = Criteria::ASC) Order by the capacity column
 * @method     ChildFacilitiesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildFacilitiesQuery orderByOpeningTime($order = Criteria::ASC) Order by the opening_time column
 * @method     ChildFacilitiesQuery orderByClosingTime($order = Criteria::ASC) Order by the closing_time column
 * @method     ChildFacilitiesQuery orderByIsDeluxe($order = Criteria::ASC) Order by the is_deluxe column
 *
 * @method     ChildFacilitiesQuery groupById() Group by the id column
 * @method     ChildFacilitiesQuery groupByName() Group by the name column
 * @method     ChildFacilitiesQuery groupByCapacity() Group by the capacity column
 * @method     ChildFacilitiesQuery groupByDescription() Group by the description column
 * @method     ChildFacilitiesQuery groupByOpeningTime() Group by the opening_time column
 * @method     ChildFacilitiesQuery groupByClosingTime() Group by the closing_time column
 * @method     ChildFacilitiesQuery groupByIsDeluxe() Group by the is_deluxe column
 *
 * @method     ChildFacilitiesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFacilitiesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFacilitiesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFacilitiesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFacilitiesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFacilitiesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFacilitiesQuery leftJoinReservations($relationAlias = null) Adds a LEFT JOIN clause to the query using the Reservations relation
 * @method     ChildFacilitiesQuery rightJoinReservations($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Reservations relation
 * @method     ChildFacilitiesQuery innerJoinReservations($relationAlias = null) Adds a INNER JOIN clause to the query using the Reservations relation
 *
 * @method     ChildFacilitiesQuery joinWithReservations($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Reservations relation
 *
 * @method     ChildFacilitiesQuery leftJoinWithReservations() Adds a LEFT JOIN clause and with to the query using the Reservations relation
 * @method     ChildFacilitiesQuery rightJoinWithReservations() Adds a RIGHT JOIN clause and with to the query using the Reservations relation
 * @method     ChildFacilitiesQuery innerJoinWithReservations() Adds a INNER JOIN clause and with to the query using the Reservations relation
 *
 * @method     \ReservationsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFacilities findOne(ConnectionInterface $con = null) Return the first ChildFacilities matching the query
 * @method     ChildFacilities findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFacilities matching the query, or a new ChildFacilities object populated from the query conditions when no match is found
 *
 * @method     ChildFacilities findOneById(int $id) Return the first ChildFacilities filtered by the id column
 * @method     ChildFacilities findOneByName(string $name) Return the first ChildFacilities filtered by the name column
 * @method     ChildFacilities findOneByCapacity(int $capacity) Return the first ChildFacilities filtered by the capacity column
 * @method     ChildFacilities findOneByDescription(string $description) Return the first ChildFacilities filtered by the description column
 * @method     ChildFacilities findOneByOpeningTime(string $opening_time) Return the first ChildFacilities filtered by the opening_time column
 * @method     ChildFacilities findOneByClosingTime(string $closing_time) Return the first ChildFacilities filtered by the closing_time column
 * @method     ChildFacilities findOneByIsDeluxe(boolean $is_deluxe) Return the first ChildFacilities filtered by the is_deluxe column *

 * @method     ChildFacilities requirePk($key, ConnectionInterface $con = null) Return the ChildFacilities by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOne(ConnectionInterface $con = null) Return the first ChildFacilities matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilities requireOneById(int $id) Return the first ChildFacilities filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByName(string $name) Return the first ChildFacilities filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByCapacity(int $capacity) Return the first ChildFacilities filtered by the capacity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByDescription(string $description) Return the first ChildFacilities filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByOpeningTime(string $opening_time) Return the first ChildFacilities filtered by the opening_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByClosingTime(string $closing_time) Return the first ChildFacilities filtered by the closing_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilities requireOneByIsDeluxe(boolean $is_deluxe) Return the first ChildFacilities filtered by the is_deluxe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilities[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFacilities objects based on current ModelCriteria
 * @method     ChildFacilities[]|ObjectCollection findById(int $id) Return ChildFacilities objects filtered by the id column
 * @method     ChildFacilities[]|ObjectCollection findByName(string $name) Return ChildFacilities objects filtered by the name column
 * @method     ChildFacilities[]|ObjectCollection findByCapacity(int $capacity) Return ChildFacilities objects filtered by the capacity column
 * @method     ChildFacilities[]|ObjectCollection findByDescription(string $description) Return ChildFacilities objects filtered by the description column
 * @method     ChildFacilities[]|ObjectCollection findByOpeningTime(string $opening_time) Return ChildFacilities objects filtered by the opening_time column
 * @method     ChildFacilities[]|ObjectCollection findByClosingTime(string $closing_time) Return ChildFacilities objects filtered by the closing_time column
 * @method     ChildFacilities[]|ObjectCollection findByIsDeluxe(boolean $is_deluxe) Return ChildFacilities objects filtered by the is_deluxe column
 * @method     ChildFacilities[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FacilitiesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FacilitiesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Facilities', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFacilitiesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFacilitiesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFacilitiesQuery) {
            return $criteria;
        }
        $query = new ChildFacilitiesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFacilities|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilitiesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FacilitiesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFacilities A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, capacity, description, opening_time, closing_time, is_deluxe FROM facilities WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFacilities $obj */
            $obj = new ChildFacilities();
            $obj->hydrate($row);
            FacilitiesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFacilities|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacilitiesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacilitiesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the capacity column
     *
     * Example usage:
     * <code>
     * $query->filterByCapacity(1234); // WHERE capacity = 1234
     * $query->filterByCapacity(array(12, 34)); // WHERE capacity IN (12, 34)
     * $query->filterByCapacity(array('min' => 12)); // WHERE capacity > 12
     * </code>
     *
     * @param     mixed $capacity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByCapacity($capacity = null, $comparison = null)
    {
        if (is_array($capacity)) {
            $useMinMax = false;
            if (isset($capacity['min'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_CAPACITY, $capacity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($capacity['max'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_CAPACITY, $capacity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_CAPACITY, $capacity, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the opening_time column
     *
     * Example usage:
     * <code>
     * $query->filterByOpeningTime('2011-03-14'); // WHERE opening_time = '2011-03-14'
     * $query->filterByOpeningTime('now'); // WHERE opening_time = '2011-03-14'
     * $query->filterByOpeningTime(array('max' => 'yesterday')); // WHERE opening_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $openingTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByOpeningTime($openingTime = null, $comparison = null)
    {
        if (is_array($openingTime)) {
            $useMinMax = false;
            if (isset($openingTime['min'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_OPENING_TIME, $openingTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($openingTime['max'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_OPENING_TIME, $openingTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_OPENING_TIME, $openingTime, $comparison);
    }

    /**
     * Filter the query on the closing_time column
     *
     * Example usage:
     * <code>
     * $query->filterByClosingTime('2011-03-14'); // WHERE closing_time = '2011-03-14'
     * $query->filterByClosingTime('now'); // WHERE closing_time = '2011-03-14'
     * $query->filterByClosingTime(array('max' => 'yesterday')); // WHERE closing_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $closingTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByClosingTime($closingTime = null, $comparison = null)
    {
        if (is_array($closingTime)) {
            $useMinMax = false;
            if (isset($closingTime['min'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_CLOSING_TIME, $closingTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($closingTime['max'])) {
                $this->addUsingAlias(FacilitiesTableMap::COL_CLOSING_TIME, $closingTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_CLOSING_TIME, $closingTime, $comparison);
    }

    /**
     * Filter the query on the is_deluxe column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeluxe(true); // WHERE is_deluxe = true
     * $query->filterByIsDeluxe('yes'); // WHERE is_deluxe = true
     * </code>
     *
     * @param     boolean|string $isDeluxe The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByIsDeluxe($isDeluxe = null, $comparison = null)
    {
        if (is_string($isDeluxe)) {
            $isDeluxe = in_array(strtolower($isDeluxe), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FacilitiesTableMap::COL_IS_DELUXE, $isDeluxe, $comparison);
    }

    /**
     * Filter the query by a related \Reservations object
     *
     * @param \Reservations|ObjectCollection $reservations the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFacilitiesQuery The current query, for fluid interface
     */
    public function filterByReservations($reservations, $comparison = null)
    {
        if ($reservations instanceof \Reservations) {
            return $this
                ->addUsingAlias(FacilitiesTableMap::COL_ID, $reservations->getFacilityId(), $comparison);
        } elseif ($reservations instanceof ObjectCollection) {
            return $this
                ->useReservationsQuery()
                ->filterByPrimaryKeys($reservations->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByReservations() only accepts arguments of type \Reservations or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Reservations relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function joinReservations($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Reservations');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Reservations');
        }

        return $this;
    }

    /**
     * Use the Reservations relation Reservations object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ReservationsQuery A secondary query class using the current class as primary query
     */
    public function useReservationsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinReservations($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Reservations', '\ReservationsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFacilities $facilities Object to remove from the list of results
     *
     * @return $this|ChildFacilitiesQuery The current query, for fluid interface
     */
    public function prune($facilities = null)
    {
        if ($facilities) {
            $this->addUsingAlias(FacilitiesTableMap::COL_ID, $facilities->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the facilities table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilitiesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FacilitiesTableMap::clearInstancePool();
            FacilitiesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilitiesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FacilitiesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FacilitiesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FacilitiesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FacilitiesQuery

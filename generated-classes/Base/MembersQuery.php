<?php

namespace Base;

use \Members as ChildMembers;
use \MembersQuery as ChildMembersQuery;
use \Exception;
use \PDO;
use Map\MembersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'members' table.
 *
 *
 *
 * @method     ChildMembersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMembersQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildMembersQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildMembersQuery orderByPlan($order = Criteria::ASC) Order by the plan column
 * @method     ChildMembersQuery orderByPlanExpiry($order = Criteria::ASC) Order by the plan_expiry column
 * @method     ChildMembersQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildMembersQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildMembersQuery orderByTrainerId($order = Criteria::ASC) Order by the trainer_id column
 *
 * @method     ChildMembersQuery groupById() Group by the id column
 * @method     ChildMembersQuery groupByFirstName() Group by the first_name column
 * @method     ChildMembersQuery groupByLastName() Group by the last_name column
 * @method     ChildMembersQuery groupByPlan() Group by the plan column
 * @method     ChildMembersQuery groupByPlanExpiry() Group by the plan_expiry column
 * @method     ChildMembersQuery groupByHeight() Group by the height column
 * @method     ChildMembersQuery groupByWeight() Group by the weight column
 * @method     ChildMembersQuery groupByTrainerId() Group by the trainer_id column
 *
 * @method     ChildMembersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMembersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMembersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMembersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMembersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMembersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMembersQuery leftJoinUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Users relation
 * @method     ChildMembersQuery rightJoinUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Users relation
 * @method     ChildMembersQuery innerJoinUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the Users relation
 *
 * @method     ChildMembersQuery joinWithUsers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Users relation
 *
 * @method     ChildMembersQuery leftJoinWithUsers() Adds a LEFT JOIN clause and with to the query using the Users relation
 * @method     ChildMembersQuery rightJoinWithUsers() Adds a RIGHT JOIN clause and with to the query using the Users relation
 * @method     ChildMembersQuery innerJoinWithUsers() Adds a INNER JOIN clause and with to the query using the Users relation
 *
 * @method     ChildMembersQuery leftJoinTrainers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trainers relation
 * @method     ChildMembersQuery rightJoinTrainers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trainers relation
 * @method     ChildMembersQuery innerJoinTrainers($relationAlias = null) Adds a INNER JOIN clause to the query using the Trainers relation
 *
 * @method     ChildMembersQuery joinWithTrainers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Trainers relation
 *
 * @method     ChildMembersQuery leftJoinWithTrainers() Adds a LEFT JOIN clause and with to the query using the Trainers relation
 * @method     ChildMembersQuery rightJoinWithTrainers() Adds a RIGHT JOIN clause and with to the query using the Trainers relation
 * @method     ChildMembersQuery innerJoinWithTrainers() Adds a INNER JOIN clause and with to the query using the Trainers relation
 *
 * @method     ChildMembersQuery leftJoinCourseMemberRegistration($relationAlias = null) Adds a LEFT JOIN clause to the query using the CourseMemberRegistration relation
 * @method     ChildMembersQuery rightJoinCourseMemberRegistration($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CourseMemberRegistration relation
 * @method     ChildMembersQuery innerJoinCourseMemberRegistration($relationAlias = null) Adds a INNER JOIN clause to the query using the CourseMemberRegistration relation
 *
 * @method     ChildMembersQuery joinWithCourseMemberRegistration($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CourseMemberRegistration relation
 *
 * @method     ChildMembersQuery leftJoinWithCourseMemberRegistration() Adds a LEFT JOIN clause and with to the query using the CourseMemberRegistration relation
 * @method     ChildMembersQuery rightJoinWithCourseMemberRegistration() Adds a RIGHT JOIN clause and with to the query using the CourseMemberRegistration relation
 * @method     ChildMembersQuery innerJoinWithCourseMemberRegistration() Adds a INNER JOIN clause and with to the query using the CourseMemberRegistration relation
 *
 * @method     ChildMembersQuery leftJoinMemberAttendance($relationAlias = null) Adds a LEFT JOIN clause to the query using the MemberAttendance relation
 * @method     ChildMembersQuery rightJoinMemberAttendance($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MemberAttendance relation
 * @method     ChildMembersQuery innerJoinMemberAttendance($relationAlias = null) Adds a INNER JOIN clause to the query using the MemberAttendance relation
 *
 * @method     ChildMembersQuery joinWithMemberAttendance($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MemberAttendance relation
 *
 * @method     ChildMembersQuery leftJoinWithMemberAttendance() Adds a LEFT JOIN clause and with to the query using the MemberAttendance relation
 * @method     ChildMembersQuery rightJoinWithMemberAttendance() Adds a RIGHT JOIN clause and with to the query using the MemberAttendance relation
 * @method     ChildMembersQuery innerJoinWithMemberAttendance() Adds a INNER JOIN clause and with to the query using the MemberAttendance relation
 *
 * @method     ChildMembersQuery leftJoinReservations($relationAlias = null) Adds a LEFT JOIN clause to the query using the Reservations relation
 * @method     ChildMembersQuery rightJoinReservations($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Reservations relation
 * @method     ChildMembersQuery innerJoinReservations($relationAlias = null) Adds a INNER JOIN clause to the query using the Reservations relation
 *
 * @method     ChildMembersQuery joinWithReservations($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Reservations relation
 *
 * @method     ChildMembersQuery leftJoinWithReservations() Adds a LEFT JOIN clause and with to the query using the Reservations relation
 * @method     ChildMembersQuery rightJoinWithReservations() Adds a RIGHT JOIN clause and with to the query using the Reservations relation
 * @method     ChildMembersQuery innerJoinWithReservations() Adds a INNER JOIN clause and with to the query using the Reservations relation
 *
 * @method     ChildMembersQuery leftJoinSchedule($relationAlias = null) Adds a LEFT JOIN clause to the query using the Schedule relation
 * @method     ChildMembersQuery rightJoinSchedule($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Schedule relation
 * @method     ChildMembersQuery innerJoinSchedule($relationAlias = null) Adds a INNER JOIN clause to the query using the Schedule relation
 *
 * @method     ChildMembersQuery joinWithSchedule($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Schedule relation
 *
 * @method     ChildMembersQuery leftJoinWithSchedule() Adds a LEFT JOIN clause and with to the query using the Schedule relation
 * @method     ChildMembersQuery rightJoinWithSchedule() Adds a RIGHT JOIN clause and with to the query using the Schedule relation
 * @method     ChildMembersQuery innerJoinWithSchedule() Adds a INNER JOIN clause and with to the query using the Schedule relation
 *
 * @method     \UsersQuery|\TrainersQuery|\CourseMemberRegistrationQuery|\MemberAttendanceQuery|\ReservationsQuery|\ScheduleQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMembers findOne(ConnectionInterface $con = null) Return the first ChildMembers matching the query
 * @method     ChildMembers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMembers matching the query, or a new ChildMembers object populated from the query conditions when no match is found
 *
 * @method     ChildMembers findOneById(int $id) Return the first ChildMembers filtered by the id column
 * @method     ChildMembers findOneByFirstName(string $first_name) Return the first ChildMembers filtered by the first_name column
 * @method     ChildMembers findOneByLastName(string $last_name) Return the first ChildMembers filtered by the last_name column
 * @method     ChildMembers findOneByPlan(string $plan) Return the first ChildMembers filtered by the plan column
 * @method     ChildMembers findOneByPlanExpiry(string $plan_expiry) Return the first ChildMembers filtered by the plan_expiry column
 * @method     ChildMembers findOneByHeight(double $height) Return the first ChildMembers filtered by the height column
 * @method     ChildMembers findOneByWeight(double $weight) Return the first ChildMembers filtered by the weight column
 * @method     ChildMembers findOneByTrainerId(int $trainer_id) Return the first ChildMembers filtered by the trainer_id column *

 * @method     ChildMembers requirePk($key, ConnectionInterface $con = null) Return the ChildMembers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOne(ConnectionInterface $con = null) Return the first ChildMembers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembers requireOneById(int $id) Return the first ChildMembers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByFirstName(string $first_name) Return the first ChildMembers filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByLastName(string $last_name) Return the first ChildMembers filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByPlan(string $plan) Return the first ChildMembers filtered by the plan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByPlanExpiry(string $plan_expiry) Return the first ChildMembers filtered by the plan_expiry column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByHeight(double $height) Return the first ChildMembers filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByWeight(double $weight) Return the first ChildMembers filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMembers requireOneByTrainerId(int $trainer_id) Return the first ChildMembers filtered by the trainer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMembers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMembers objects based on current ModelCriteria
 * @method     ChildMembers[]|ObjectCollection findById(int $id) Return ChildMembers objects filtered by the id column
 * @method     ChildMembers[]|ObjectCollection findByFirstName(string $first_name) Return ChildMembers objects filtered by the first_name column
 * @method     ChildMembers[]|ObjectCollection findByLastName(string $last_name) Return ChildMembers objects filtered by the last_name column
 * @method     ChildMembers[]|ObjectCollection findByPlan(string $plan) Return ChildMembers objects filtered by the plan column
 * @method     ChildMembers[]|ObjectCollection findByPlanExpiry(string $plan_expiry) Return ChildMembers objects filtered by the plan_expiry column
 * @method     ChildMembers[]|ObjectCollection findByHeight(double $height) Return ChildMembers objects filtered by the height column
 * @method     ChildMembers[]|ObjectCollection findByWeight(double $weight) Return ChildMembers objects filtered by the weight column
 * @method     ChildMembers[]|ObjectCollection findByTrainerId(int $trainer_id) Return ChildMembers objects filtered by the trainer_id column
 * @method     ChildMembers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MembersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MembersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Members', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMembersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMembersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMembersQuery) {
            return $criteria;
        }
        $query = new ChildMembersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMembers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MembersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MembersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, first_name, last_name, plan, plan_expiry, height, weight, trainer_id FROM members WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMembers $obj */
            $obj = new ChildMembers();
            $obj->hydrate($row);
            MembersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMembers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MembersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MembersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @see       filterByUsers()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MembersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MembersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%'); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstName)) {
                $firstName = str_replace('*', '%', $firstName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%'); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastName)) {
                $lastName = str_replace('*', '%', $lastName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the plan column
     *
     * Example usage:
     * <code>
     * $query->filterByPlan('fooValue');   // WHERE plan = 'fooValue'
     * $query->filterByPlan('%fooValue%'); // WHERE plan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $plan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByPlan($plan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($plan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $plan)) {
                $plan = str_replace('*', '%', $plan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_PLAN, $plan, $comparison);
    }

    /**
     * Filter the query on the plan_expiry column
     *
     * Example usage:
     * <code>
     * $query->filterByPlanExpiry('2011-03-14'); // WHERE plan_expiry = '2011-03-14'
     * $query->filterByPlanExpiry('now'); // WHERE plan_expiry = '2011-03-14'
     * $query->filterByPlanExpiry(array('max' => 'yesterday')); // WHERE plan_expiry > '2011-03-13'
     * </code>
     *
     * @param     mixed $planExpiry The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByPlanExpiry($planExpiry = null, $comparison = null)
    {
        if (is_array($planExpiry)) {
            $useMinMax = false;
            if (isset($planExpiry['min'])) {
                $this->addUsingAlias(MembersTableMap::COL_PLAN_EXPIRY, $planExpiry['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($planExpiry['max'])) {
                $this->addUsingAlias(MembersTableMap::COL_PLAN_EXPIRY, $planExpiry['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_PLAN_EXPIRY, $planExpiry, $comparison);
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight(1234); // WHERE height = 1234
     * $query->filterByHeight(array(12, 34)); // WHERE height IN (12, 34)
     * $query->filterByHeight(array('min' => 12)); // WHERE height > 12
     * </code>
     *
     * @param     mixed $height The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByHeight($height = null, $comparison = null)
    {
        if (is_array($height)) {
            $useMinMax = false;
            if (isset($height['min'])) {
                $this->addUsingAlias(MembersTableMap::COL_HEIGHT, $height['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($height['max'])) {
                $this->addUsingAlias(MembersTableMap::COL_HEIGHT, $height['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_HEIGHT, $height, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(MembersTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(MembersTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the trainer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTrainerId(1234); // WHERE trainer_id = 1234
     * $query->filterByTrainerId(array(12, 34)); // WHERE trainer_id IN (12, 34)
     * $query->filterByTrainerId(array('min' => 12)); // WHERE trainer_id > 12
     * </code>
     *
     * @see       filterByTrainers()
     *
     * @param     mixed $trainerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function filterByTrainerId($trainerId = null, $comparison = null)
    {
        if (is_array($trainerId)) {
            $useMinMax = false;
            if (isset($trainerId['min'])) {
                $this->addUsingAlias(MembersTableMap::COL_TRAINER_ID, $trainerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trainerId['max'])) {
                $this->addUsingAlias(MembersTableMap::COL_TRAINER_ID, $trainerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MembersTableMap::COL_TRAINER_ID, $trainerId, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterByUsers($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsers() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinUsers($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Users');
        }

        return $this;
    }

    /**
     * Use the Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Users', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Trainers object
     *
     * @param \Trainers|ObjectCollection $trainers The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterByTrainers($trainers, $comparison = null)
    {
        if ($trainers instanceof \Trainers) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_TRAINER_ID, $trainers->getId(), $comparison);
        } elseif ($trainers instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MembersTableMap::COL_TRAINER_ID, $trainers->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTrainers() only accepts arguments of type \Trainers or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Trainers relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinTrainers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Trainers');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Trainers');
        }

        return $this;
    }

    /**
     * Use the Trainers relation Trainers object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TrainersQuery A secondary query class using the current class as primary query
     */
    public function useTrainersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTrainers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Trainers', '\TrainersQuery');
    }

    /**
     * Filter the query by a related \CourseMemberRegistration object
     *
     * @param \CourseMemberRegistration|ObjectCollection $courseMemberRegistration the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterByCourseMemberRegistration($courseMemberRegistration, $comparison = null)
    {
        if ($courseMemberRegistration instanceof \CourseMemberRegistration) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $courseMemberRegistration->getMemberId(), $comparison);
        } elseif ($courseMemberRegistration instanceof ObjectCollection) {
            return $this
                ->useCourseMemberRegistrationQuery()
                ->filterByPrimaryKeys($courseMemberRegistration->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourseMemberRegistration() only accepts arguments of type \CourseMemberRegistration or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CourseMemberRegistration relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinCourseMemberRegistration($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CourseMemberRegistration');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CourseMemberRegistration');
        }

        return $this;
    }

    /**
     * Use the CourseMemberRegistration relation CourseMemberRegistration object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CourseMemberRegistrationQuery A secondary query class using the current class as primary query
     */
    public function useCourseMemberRegistrationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourseMemberRegistration($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CourseMemberRegistration', '\CourseMemberRegistrationQuery');
    }

    /**
     * Filter the query by a related \MemberAttendance object
     *
     * @param \MemberAttendance|ObjectCollection $memberAttendance the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterByMemberAttendance($memberAttendance, $comparison = null)
    {
        if ($memberAttendance instanceof \MemberAttendance) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $memberAttendance->getMemberId(), $comparison);
        } elseif ($memberAttendance instanceof ObjectCollection) {
            return $this
                ->useMemberAttendanceQuery()
                ->filterByPrimaryKeys($memberAttendance->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMemberAttendance() only accepts arguments of type \MemberAttendance or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MemberAttendance relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinMemberAttendance($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MemberAttendance');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MemberAttendance');
        }

        return $this;
    }

    /**
     * Use the MemberAttendance relation MemberAttendance object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MemberAttendanceQuery A secondary query class using the current class as primary query
     */
    public function useMemberAttendanceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMemberAttendance($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MemberAttendance', '\MemberAttendanceQuery');
    }

    /**
     * Filter the query by a related \Reservations object
     *
     * @param \Reservations|ObjectCollection $reservations the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterByReservations($reservations, $comparison = null)
    {
        if ($reservations instanceof \Reservations) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $reservations->getMemberId(), $comparison);
        } elseif ($reservations instanceof ObjectCollection) {
            return $this
                ->useReservationsQuery()
                ->filterByPrimaryKeys($reservations->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByReservations() only accepts arguments of type \Reservations or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Reservations relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinReservations($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Reservations');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Reservations');
        }

        return $this;
    }

    /**
     * Use the Reservations relation Reservations object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ReservationsQuery A secondary query class using the current class as primary query
     */
    public function useReservationsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinReservations($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Reservations', '\ReservationsQuery');
    }

    /**
     * Filter the query by a related \Schedule object
     *
     * @param \Schedule|ObjectCollection $schedule the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMembersQuery The current query, for fluid interface
     */
    public function filterBySchedule($schedule, $comparison = null)
    {
        if ($schedule instanceof \Schedule) {
            return $this
                ->addUsingAlias(MembersTableMap::COL_ID, $schedule->getMemberId(), $comparison);
        } elseif ($schedule instanceof ObjectCollection) {
            return $this
                ->useScheduleQuery()
                ->filterByPrimaryKeys($schedule->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySchedule() only accepts arguments of type \Schedule or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Schedule relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function joinSchedule($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Schedule');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Schedule');
        }

        return $this;
    }

    /**
     * Use the Schedule relation Schedule object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ScheduleQuery A secondary query class using the current class as primary query
     */
    public function useScheduleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSchedule($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Schedule', '\ScheduleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMembers $members Object to remove from the list of results
     *
     * @return $this|ChildMembersQuery The current query, for fluid interface
     */
    public function prune($members = null)
    {
        if ($members) {
            $this->addUsingAlias(MembersTableMap::COL_ID, $members->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the members table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MembersTableMap::clearInstancePool();
            MembersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MembersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MembersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MembersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MembersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MembersQuery

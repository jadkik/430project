<?php

namespace Base;

use \Trainers as ChildTrainers;
use \TrainersQuery as ChildTrainersQuery;
use \Exception;
use \PDO;
use Map\TrainersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'trainers' table.
 *
 *
 *
 * @method     ChildTrainersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTrainersQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildTrainersQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildTrainersQuery orderByHoursRequired($order = Criteria::ASC) Order by the hours_required column
 * @method     ChildTrainersQuery orderByManagerId($order = Criteria::ASC) Order by the manager_id column
 *
 * @method     ChildTrainersQuery groupById() Group by the id column
 * @method     ChildTrainersQuery groupByFirstName() Group by the first_name column
 * @method     ChildTrainersQuery groupByLastName() Group by the last_name column
 * @method     ChildTrainersQuery groupByHoursRequired() Group by the hours_required column
 * @method     ChildTrainersQuery groupByManagerId() Group by the manager_id column
 *
 * @method     ChildTrainersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTrainersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTrainersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTrainersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTrainersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTrainersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTrainersQuery leftJoinUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Users relation
 * @method     ChildTrainersQuery rightJoinUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Users relation
 * @method     ChildTrainersQuery innerJoinUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the Users relation
 *
 * @method     ChildTrainersQuery joinWithUsers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Users relation
 *
 * @method     ChildTrainersQuery leftJoinWithUsers() Adds a LEFT JOIN clause and with to the query using the Users relation
 * @method     ChildTrainersQuery rightJoinWithUsers() Adds a RIGHT JOIN clause and with to the query using the Users relation
 * @method     ChildTrainersQuery innerJoinWithUsers() Adds a INNER JOIN clause and with to the query using the Users relation
 *
 * @method     ChildTrainersQuery leftJoinManagers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Managers relation
 * @method     ChildTrainersQuery rightJoinManagers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Managers relation
 * @method     ChildTrainersQuery innerJoinManagers($relationAlias = null) Adds a INNER JOIN clause to the query using the Managers relation
 *
 * @method     ChildTrainersQuery joinWithManagers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Managers relation
 *
 * @method     ChildTrainersQuery leftJoinWithManagers() Adds a LEFT JOIN clause and with to the query using the Managers relation
 * @method     ChildTrainersQuery rightJoinWithManagers() Adds a RIGHT JOIN clause and with to the query using the Managers relation
 * @method     ChildTrainersQuery innerJoinWithManagers() Adds a INNER JOIN clause and with to the query using the Managers relation
 *
 * @method     ChildTrainersQuery leftJoinCourses($relationAlias = null) Adds a LEFT JOIN clause to the query using the Courses relation
 * @method     ChildTrainersQuery rightJoinCourses($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Courses relation
 * @method     ChildTrainersQuery innerJoinCourses($relationAlias = null) Adds a INNER JOIN clause to the query using the Courses relation
 *
 * @method     ChildTrainersQuery joinWithCourses($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Courses relation
 *
 * @method     ChildTrainersQuery leftJoinWithCourses() Adds a LEFT JOIN clause and with to the query using the Courses relation
 * @method     ChildTrainersQuery rightJoinWithCourses() Adds a RIGHT JOIN clause and with to the query using the Courses relation
 * @method     ChildTrainersQuery innerJoinWithCourses() Adds a INNER JOIN clause and with to the query using the Courses relation
 *
 * @method     ChildTrainersQuery leftJoinMembers($relationAlias = null) Adds a LEFT JOIN clause to the query using the Members relation
 * @method     ChildTrainersQuery rightJoinMembers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Members relation
 * @method     ChildTrainersQuery innerJoinMembers($relationAlias = null) Adds a INNER JOIN clause to the query using the Members relation
 *
 * @method     ChildTrainersQuery joinWithMembers($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Members relation
 *
 * @method     ChildTrainersQuery leftJoinWithMembers() Adds a LEFT JOIN clause and with to the query using the Members relation
 * @method     ChildTrainersQuery rightJoinWithMembers() Adds a RIGHT JOIN clause and with to the query using the Members relation
 * @method     ChildTrainersQuery innerJoinWithMembers() Adds a INNER JOIN clause and with to the query using the Members relation
 *
 * @method     ChildTrainersQuery leftJoinTrainerAttendance($relationAlias = null) Adds a LEFT JOIN clause to the query using the TrainerAttendance relation
 * @method     ChildTrainersQuery rightJoinTrainerAttendance($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TrainerAttendance relation
 * @method     ChildTrainersQuery innerJoinTrainerAttendance($relationAlias = null) Adds a INNER JOIN clause to the query using the TrainerAttendance relation
 *
 * @method     ChildTrainersQuery joinWithTrainerAttendance($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TrainerAttendance relation
 *
 * @method     ChildTrainersQuery leftJoinWithTrainerAttendance() Adds a LEFT JOIN clause and with to the query using the TrainerAttendance relation
 * @method     ChildTrainersQuery rightJoinWithTrainerAttendance() Adds a RIGHT JOIN clause and with to the query using the TrainerAttendance relation
 * @method     ChildTrainersQuery innerJoinWithTrainerAttendance() Adds a INNER JOIN clause and with to the query using the TrainerAttendance relation
 *
 * @method     \UsersQuery|\ManagersQuery|\CoursesQuery|\MembersQuery|\TrainerAttendanceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTrainers findOne(ConnectionInterface $con = null) Return the first ChildTrainers matching the query
 * @method     ChildTrainers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTrainers matching the query, or a new ChildTrainers object populated from the query conditions when no match is found
 *
 * @method     ChildTrainers findOneById(int $id) Return the first ChildTrainers filtered by the id column
 * @method     ChildTrainers findOneByFirstName(string $first_name) Return the first ChildTrainers filtered by the first_name column
 * @method     ChildTrainers findOneByLastName(string $last_name) Return the first ChildTrainers filtered by the last_name column
 * @method     ChildTrainers findOneByHoursRequired(int $hours_required) Return the first ChildTrainers filtered by the hours_required column
 * @method     ChildTrainers findOneByManagerId(int $manager_id) Return the first ChildTrainers filtered by the manager_id column *

 * @method     ChildTrainers requirePk($key, ConnectionInterface $con = null) Return the ChildTrainers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTrainers requireOne(ConnectionInterface $con = null) Return the first ChildTrainers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTrainers requireOneById(int $id) Return the first ChildTrainers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTrainers requireOneByFirstName(string $first_name) Return the first ChildTrainers filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTrainers requireOneByLastName(string $last_name) Return the first ChildTrainers filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTrainers requireOneByHoursRequired(int $hours_required) Return the first ChildTrainers filtered by the hours_required column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTrainers requireOneByManagerId(int $manager_id) Return the first ChildTrainers filtered by the manager_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTrainers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTrainers objects based on current ModelCriteria
 * @method     ChildTrainers[]|ObjectCollection findById(int $id) Return ChildTrainers objects filtered by the id column
 * @method     ChildTrainers[]|ObjectCollection findByFirstName(string $first_name) Return ChildTrainers objects filtered by the first_name column
 * @method     ChildTrainers[]|ObjectCollection findByLastName(string $last_name) Return ChildTrainers objects filtered by the last_name column
 * @method     ChildTrainers[]|ObjectCollection findByHoursRequired(int $hours_required) Return ChildTrainers objects filtered by the hours_required column
 * @method     ChildTrainers[]|ObjectCollection findByManagerId(int $manager_id) Return ChildTrainers objects filtered by the manager_id column
 * @method     ChildTrainers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TrainersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TrainersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Trainers', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTrainersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTrainersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTrainersQuery) {
            return $criteria;
        }
        $query = new ChildTrainersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTrainers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TrainersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TrainersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTrainers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, first_name, last_name, hours_required, manager_id FROM trainers WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTrainers $obj */
            $obj = new ChildTrainers();
            $obj->hydrate($row);
            TrainersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTrainers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TrainersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TrainersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @see       filterByUsers()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TrainersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TrainersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%'); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstName)) {
                $firstName = str_replace('*', '%', $firstName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainersTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%'); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastName)) {
                $lastName = str_replace('*', '%', $lastName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainersTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the hours_required column
     *
     * Example usage:
     * <code>
     * $query->filterByHoursRequired(1234); // WHERE hours_required = 1234
     * $query->filterByHoursRequired(array(12, 34)); // WHERE hours_required IN (12, 34)
     * $query->filterByHoursRequired(array('min' => 12)); // WHERE hours_required > 12
     * </code>
     *
     * @param     mixed $hoursRequired The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByHoursRequired($hoursRequired = null, $comparison = null)
    {
        if (is_array($hoursRequired)) {
            $useMinMax = false;
            if (isset($hoursRequired['min'])) {
                $this->addUsingAlias(TrainersTableMap::COL_HOURS_REQUIRED, $hoursRequired['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hoursRequired['max'])) {
                $this->addUsingAlias(TrainersTableMap::COL_HOURS_REQUIRED, $hoursRequired['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainersTableMap::COL_HOURS_REQUIRED, $hoursRequired, $comparison);
    }

    /**
     * Filter the query on the manager_id column
     *
     * Example usage:
     * <code>
     * $query->filterByManagerId(1234); // WHERE manager_id = 1234
     * $query->filterByManagerId(array(12, 34)); // WHERE manager_id IN (12, 34)
     * $query->filterByManagerId(array('min' => 12)); // WHERE manager_id > 12
     * </code>
     *
     * @see       filterByManagers()
     *
     * @param     mixed $managerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByManagerId($managerId = null, $comparison = null)
    {
        if (is_array($managerId)) {
            $useMinMax = false;
            if (isset($managerId['min'])) {
                $this->addUsingAlias(TrainersTableMap::COL_MANAGER_ID, $managerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($managerId['max'])) {
                $this->addUsingAlias(TrainersTableMap::COL_MANAGER_ID, $managerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainersTableMap::COL_MANAGER_ID, $managerId, $comparison);
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByUsers($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(TrainersTableMap::COL_ID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TrainersTableMap::COL_ID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsers() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function joinUsers($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Users');
        }

        return $this;
    }

    /**
     * Use the Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Users', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Managers object
     *
     * @param \Managers|ObjectCollection $managers The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByManagers($managers, $comparison = null)
    {
        if ($managers instanceof \Managers) {
            return $this
                ->addUsingAlias(TrainersTableMap::COL_MANAGER_ID, $managers->getId(), $comparison);
        } elseif ($managers instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TrainersTableMap::COL_MANAGER_ID, $managers->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByManagers() only accepts arguments of type \Managers or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Managers relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function joinManagers($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Managers');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Managers');
        }

        return $this;
    }

    /**
     * Use the Managers relation Managers object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ManagersQuery A secondary query class using the current class as primary query
     */
    public function useManagersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinManagers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Managers', '\ManagersQuery');
    }

    /**
     * Filter the query by a related \Courses object
     *
     * @param \Courses|ObjectCollection $courses the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByCourses($courses, $comparison = null)
    {
        if ($courses instanceof \Courses) {
            return $this
                ->addUsingAlias(TrainersTableMap::COL_ID, $courses->getTrainerId(), $comparison);
        } elseif ($courses instanceof ObjectCollection) {
            return $this
                ->useCoursesQuery()
                ->filterByPrimaryKeys($courses->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCourses() only accepts arguments of type \Courses or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Courses relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function joinCourses($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Courses');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Courses');
        }

        return $this;
    }

    /**
     * Use the Courses relation Courses object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoursesQuery A secondary query class using the current class as primary query
     */
    public function useCoursesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCourses($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Courses', '\CoursesQuery');
    }

    /**
     * Filter the query by a related \Members object
     *
     * @param \Members|ObjectCollection $members the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByMembers($members, $comparison = null)
    {
        if ($members instanceof \Members) {
            return $this
                ->addUsingAlias(TrainersTableMap::COL_ID, $members->getTrainerId(), $comparison);
        } elseif ($members instanceof ObjectCollection) {
            return $this
                ->useMembersQuery()
                ->filterByPrimaryKeys($members->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMembers() only accepts arguments of type \Members or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Members relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function joinMembers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Members');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Members');
        }

        return $this;
    }

    /**
     * Use the Members relation Members object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MembersQuery A secondary query class using the current class as primary query
     */
    public function useMembersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMembers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Members', '\MembersQuery');
    }

    /**
     * Filter the query by a related \TrainerAttendance object
     *
     * @param \TrainerAttendance|ObjectCollection $trainerAttendance the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTrainersQuery The current query, for fluid interface
     */
    public function filterByTrainerAttendance($trainerAttendance, $comparison = null)
    {
        if ($trainerAttendance instanceof \TrainerAttendance) {
            return $this
                ->addUsingAlias(TrainersTableMap::COL_ID, $trainerAttendance->getTrainerId(), $comparison);
        } elseif ($trainerAttendance instanceof ObjectCollection) {
            return $this
                ->useTrainerAttendanceQuery()
                ->filterByPrimaryKeys($trainerAttendance->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTrainerAttendance() only accepts arguments of type \TrainerAttendance or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TrainerAttendance relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function joinTrainerAttendance($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TrainerAttendance');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TrainerAttendance');
        }

        return $this;
    }

    /**
     * Use the TrainerAttendance relation TrainerAttendance object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TrainerAttendanceQuery A secondary query class using the current class as primary query
     */
    public function useTrainerAttendanceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTrainerAttendance($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TrainerAttendance', '\TrainerAttendanceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTrainers $trainers Object to remove from the list of results
     *
     * @return $this|ChildTrainersQuery The current query, for fluid interface
     */
    public function prune($trainers = null)
    {
        if ($trainers) {
            $this->addUsingAlias(TrainersTableMap::COL_ID, $trainers->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the trainers table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TrainersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TrainersTableMap::clearInstancePool();
            TrainersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TrainersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TrainersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TrainersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TrainersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TrainersQuery

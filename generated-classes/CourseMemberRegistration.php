<?php

use Base\CourseMemberRegistration as BaseCourseMemberRegistration;

/**
 * Skeleton subclass for representing a row from the 'course_member_registration' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CourseMemberRegistration extends BaseCourseMemberRegistration
{

}

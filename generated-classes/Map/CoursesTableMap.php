<?php

namespace Map;

use \Courses;
use \CoursesQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'courses' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CoursesTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CoursesTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'courses';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Courses';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Courses';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'courses.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'courses.name';

    /**
     * the column name for the trainer_id field
     */
    const COL_TRAINER_ID = 'courses.trainer_id';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'courses.description';

    /**
     * the column name for the start_time field
     */
    const COL_START_TIME = 'courses.start_time';

    /**
     * the column name for the end_time field
     */
    const COL_END_TIME = 'courses.end_time';

    /**
     * the column name for the is_deluxe field
     */
    const COL_IS_DELUXE = 'courses.is_deluxe';

    /**
     * the column name for the weekdays field
     */
    const COL_WEEKDAYS = 'courses.weekdays';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'TrainerId', 'Description', 'StartTime', 'EndTime', 'IsDeluxe', 'Weekdays', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'trainerId', 'description', 'startTime', 'endTime', 'isDeluxe', 'weekdays', ),
        self::TYPE_COLNAME       => array(CoursesTableMap::COL_ID, CoursesTableMap::COL_NAME, CoursesTableMap::COL_TRAINER_ID, CoursesTableMap::COL_DESCRIPTION, CoursesTableMap::COL_START_TIME, CoursesTableMap::COL_END_TIME, CoursesTableMap::COL_IS_DELUXE, CoursesTableMap::COL_WEEKDAYS, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'trainer_id', 'description', 'start_time', 'end_time', 'is_deluxe', 'weekdays', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'TrainerId' => 2, 'Description' => 3, 'StartTime' => 4, 'EndTime' => 5, 'IsDeluxe' => 6, 'Weekdays' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'trainerId' => 2, 'description' => 3, 'startTime' => 4, 'endTime' => 5, 'isDeluxe' => 6, 'weekdays' => 7, ),
        self::TYPE_COLNAME       => array(CoursesTableMap::COL_ID => 0, CoursesTableMap::COL_NAME => 1, CoursesTableMap::COL_TRAINER_ID => 2, CoursesTableMap::COL_DESCRIPTION => 3, CoursesTableMap::COL_START_TIME => 4, CoursesTableMap::COL_END_TIME => 5, CoursesTableMap::COL_IS_DELUXE => 6, CoursesTableMap::COL_WEEKDAYS => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'trainer_id' => 2, 'description' => 3, 'start_time' => 4, 'end_time' => 5, 'is_deluxe' => 6, 'weekdays' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('courses');
        $this->setPhpName('Courses');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Courses');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 255, null);
        $this->addForeignKey('trainer_id', 'TrainerId', 'INTEGER', 'trainers', 'id', true, 10, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('start_time', 'StartTime', 'TIME', false, null, null);
        $this->addColumn('end_time', 'EndTime', 'TIME', false, null, null);
        $this->addColumn('is_deluxe', 'IsDeluxe', 'BOOLEAN', true, 1, false);
        $this->addColumn('weekdays', 'Weekdays', 'CHAR', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Trainers', '\\Trainers', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':trainer_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CourseMemberRegistration', '\\CourseMemberRegistration', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':course_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'CourseMemberRegistrations', false);
        $this->addRelation('MemberAttendance', '\\MemberAttendance', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':course_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'MemberAttendances', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to courses     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CourseMemberRegistrationTableMap::clearInstancePool();
        MemberAttendanceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CoursesTableMap::CLASS_DEFAULT : CoursesTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Courses object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CoursesTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CoursesTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CoursesTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CoursesTableMap::OM_CLASS;
            /** @var Courses $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CoursesTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CoursesTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CoursesTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Courses $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CoursesTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CoursesTableMap::COL_ID);
            $criteria->addSelectColumn(CoursesTableMap::COL_NAME);
            $criteria->addSelectColumn(CoursesTableMap::COL_TRAINER_ID);
            $criteria->addSelectColumn(CoursesTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(CoursesTableMap::COL_START_TIME);
            $criteria->addSelectColumn(CoursesTableMap::COL_END_TIME);
            $criteria->addSelectColumn(CoursesTableMap::COL_IS_DELUXE);
            $criteria->addSelectColumn(CoursesTableMap::COL_WEEKDAYS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.trainer_id');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.start_time');
            $criteria->addSelectColumn($alias . '.end_time');
            $criteria->addSelectColumn($alias . '.is_deluxe');
            $criteria->addSelectColumn($alias . '.weekdays');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CoursesTableMap::DATABASE_NAME)->getTable(CoursesTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CoursesTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CoursesTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CoursesTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Courses or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Courses object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Courses) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CoursesTableMap::DATABASE_NAME);
            $criteria->add(CoursesTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CoursesQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CoursesTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CoursesTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the courses table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CoursesQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Courses or Criteria object.
     *
     * @param mixed               $criteria Criteria or Courses object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CoursesTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Courses object
        }

        if ($criteria->containsKey(CoursesTableMap::COL_ID) && $criteria->keyContainsValue(CoursesTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CoursesTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CoursesQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CoursesTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CoursesTableMap::buildTableMap();

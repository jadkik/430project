<?php

namespace Map;

use \Facilities;
use \FacilitiesQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'facilities' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FacilitiesTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.FacilitiesTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'facilities';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Facilities';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Facilities';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'facilities.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'facilities.name';

    /**
     * the column name for the capacity field
     */
    const COL_CAPACITY = 'facilities.capacity';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'facilities.description';

    /**
     * the column name for the opening_time field
     */
    const COL_OPENING_TIME = 'facilities.opening_time';

    /**
     * the column name for the closing_time field
     */
    const COL_CLOSING_TIME = 'facilities.closing_time';

    /**
     * the column name for the is_deluxe field
     */
    const COL_IS_DELUXE = 'facilities.is_deluxe';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Capacity', 'Description', 'OpeningTime', 'ClosingTime', 'IsDeluxe', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'capacity', 'description', 'openingTime', 'closingTime', 'isDeluxe', ),
        self::TYPE_COLNAME       => array(FacilitiesTableMap::COL_ID, FacilitiesTableMap::COL_NAME, FacilitiesTableMap::COL_CAPACITY, FacilitiesTableMap::COL_DESCRIPTION, FacilitiesTableMap::COL_OPENING_TIME, FacilitiesTableMap::COL_CLOSING_TIME, FacilitiesTableMap::COL_IS_DELUXE, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'capacity', 'description', 'opening_time', 'closing_time', 'is_deluxe', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Capacity' => 2, 'Description' => 3, 'OpeningTime' => 4, 'ClosingTime' => 5, 'IsDeluxe' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'capacity' => 2, 'description' => 3, 'openingTime' => 4, 'closingTime' => 5, 'isDeluxe' => 6, ),
        self::TYPE_COLNAME       => array(FacilitiesTableMap::COL_ID => 0, FacilitiesTableMap::COL_NAME => 1, FacilitiesTableMap::COL_CAPACITY => 2, FacilitiesTableMap::COL_DESCRIPTION => 3, FacilitiesTableMap::COL_OPENING_TIME => 4, FacilitiesTableMap::COL_CLOSING_TIME => 5, FacilitiesTableMap::COL_IS_DELUXE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'capacity' => 2, 'description' => 3, 'opening_time' => 4, 'closing_time' => 5, 'is_deluxe' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('facilities');
        $this->setPhpName('Facilities');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Facilities');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('capacity', 'Capacity', 'INTEGER', true, 10, 0);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('opening_time', 'OpeningTime', 'TIME', true, null, null);
        $this->addColumn('closing_time', 'ClosingTime', 'TIME', true, null, null);
        $this->addColumn('is_deluxe', 'IsDeluxe', 'BOOLEAN', true, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Reservations', '\\Reservations', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':facility_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'Reservationss', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to facilities     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ReservationsTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FacilitiesTableMap::CLASS_DEFAULT : FacilitiesTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Facilities object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FacilitiesTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FacilitiesTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FacilitiesTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FacilitiesTableMap::OM_CLASS;
            /** @var Facilities $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FacilitiesTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FacilitiesTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FacilitiesTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Facilities $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FacilitiesTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FacilitiesTableMap::COL_ID);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_NAME);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_CAPACITY);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_OPENING_TIME);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_CLOSING_TIME);
            $criteria->addSelectColumn(FacilitiesTableMap::COL_IS_DELUXE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.capacity');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.opening_time');
            $criteria->addSelectColumn($alias . '.closing_time');
            $criteria->addSelectColumn($alias . '.is_deluxe');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FacilitiesTableMap::DATABASE_NAME)->getTable(FacilitiesTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FacilitiesTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FacilitiesTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FacilitiesTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Facilities or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Facilities object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilitiesTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Facilities) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FacilitiesTableMap::DATABASE_NAME);
            $criteria->add(FacilitiesTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = FacilitiesQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FacilitiesTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FacilitiesTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the facilities table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FacilitiesQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Facilities or Criteria object.
     *
     * @param mixed               $criteria Criteria or Facilities object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilitiesTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Facilities object
        }

        if ($criteria->containsKey(FacilitiesTableMap::COL_ID) && $criteria->keyContainsValue(FacilitiesTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.FacilitiesTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = FacilitiesQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FacilitiesTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FacilitiesTableMap::buildTableMap();

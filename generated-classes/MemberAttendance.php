<?php

use Base\MemberAttendance as BaseMemberAttendance;

/**
 * Skeleton subclass for representing a row from the 'member_attendance' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MemberAttendance extends BaseMemberAttendance
{

}

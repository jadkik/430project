<?php

use Base\Members as BaseMembers;

/**
 * Skeleton subclass for representing a row from the 'members' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Members extends BaseMembers
{
    function hasDeluxePlan() {
        $plan = $this->getPlan();
        return ($plan === 'deluxe_monthly') || ($plan === 'deluxe_yearly');
    }
    
    function hasPlanExpired() {
        $expiry = $this->getPlanExpiry();
        $today = new DateTime();
        return $today > $expiry;
    }
    
    function getPlanDescription() {
        switch ($this->getPlan()) {
            case 'basic':
                return 'This is the basic monthly plan.';
            case 'deluxe_monthly':
                return 'This is the deluxe monthly plan. You get access to all deluxe facilities for a month.';
            case 'deluxe_yearly':
                return 'This is the deluxe yearly plan. You get access to all deluxe facilities for a year.';
        }
        return '';
    }
}

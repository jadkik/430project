<?php

use Base\TrainerAttendanceQuery as BaseTrainerAttendanceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'trainer_attendance' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TrainerAttendanceQuery extends BaseTrainerAttendanceQuery
{

}

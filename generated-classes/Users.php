<?php

use Base\Users as BaseUsers;

/**
 * Skeleton subclass for representing a row from the 'users' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Users extends BaseUsers
{
    public static function displayName($user) {
        $role = $user? $user->getRole() : "";
        switch ($role) {
            case 'manager':
                $m = (new ManagersQuery())->findPk($user->getId());
                return "{$m->getFirstName()} {$m->getLastName()}";
            case 'member':
                $m = (new MembersQuery())->findPk($user->getId());
                return "{$m->getFirstName()} {$m->getLastName()}";
            case 'trainer':
                $m = (new TrainersQuery())->findPk($user->getId());
                return "{$m->getFirstName()} {$m->getLastName()}";
            default:
                return "Anonymous";
        }
    }

}

<?php
/*File almost included in all php files
Contains functions that behave a controller (MVC-like)
Also contains functions that utilize AdminLTE css for notifications
By Kik
*/
require __DIR__ . '/vendor/autoload.php';

// setup Propel
require_once __DIR__ . '/generated-conf/config.php';

require_once __DIR__ . '/Editor-PHP-1.5.5/php/DataTables.php';

session_start();
//Inverts colors so that when displaying a schedule, the text displayed on top if it could be clearly read... edit by Nappa
function color_inverse($color){
    $color = str_replace('#', '', $color);
    if (strlen($color) != 6){ return '000000'; }
    $rgb = '';
    for ($x=0;$x<3;$x++){
        $c = 255 - hexdec(substr($color,(2*$x),2));
        $c = ($c < 0) ? 0 : dechex($c);
        $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
    }
    return '#'.$rgb;
}


function view($file, $data=array()) {
    extract($data);
    $user = get_logged_in_user();
    include __DIR__ . '/views/' . $file . '.php';
}
//Notification after succesful/failed login or when trying to cause havoc in the system, alerting module (sort of)
//Edited By Kik, fixed the missing variable bug
function flash_tits($message, $level='info') {
    if (!isset($_SESSION['flashed_messages'])) {
        $_SESSION['flashed_messages'] = [];
    }
    $_SESSION['flashed_messages'][] = [$level, $message];
}

function get_flashed_tits() {
    if (!isset($_SESSION['flashed_messages'])) {
        return [];
    }
    $s = $_SESSION['flashed_messages'];
    unset($_SESSION['flashed_messages']);
    return $s;
}

$ICONS = array(
    'danger' => 'ban',
    'info' => 'info',
    'success' => 'check',
    'warning' => 'warning',
);
//Helper function for the alert module above
//Modified to support AdminLTE notifications
function render_flashed_tits() {
    global $ICONS;
    $messages = get_flashed_tits();
    if ($messages) {
        foreach ($messages as $msg) {
            error_log($msg[0]);
            echo '<div class="alert alert-' . $msg[0] . ' alert-dismissible"> ';
            echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ';
            echo '<strong><i class="icon fa fa-' . $ICONS[$msg[0]] . '"></i> ' . ucwords($msg[0]) . '</strong> ';
            echo htmlentities($msg[1]);
            echo '</div>';
        }
    }
}

function redirect($url) {
    header('Location: ' . $url);
    exit;
}

function get_logged_in_user($role='') {
    if (!isset($_SESSION['user_id'])) {
        return NULL;
    }
    $id = $_SESSION['user_id'];
    $user = (new UsersQuery())->findPk($id);
    if (empty($user)) {
        return NULL;
    }
    if (!empty($role) && $role !== $user->getRole()) {
        return NULL;
    }
    return $user;
}

<?php
//Simple index page will be viewed as the homePage
//By Nappa
require_once __DIR__ . '/include.php';

//~ if (!get_logged_in_user()) {
    //~ redirect('login.php');
//~ }

$data['new_notifications_count'] = 0;

$user = get_logged_in_user();
if (!empty($user)) {
    $data['new_notifications_count'] = 5;
    $data['notifications'][] = '<i class="fa fa-users text-aqua"></i> 5 new members joined today';
    $data['notifications'][] = '<i class="fa fa-users text-aqua"></i> 4 new members joined today';
    $data['notifications'][] = '<i class="fa fa-users text-aqua"></i> 3 new members joined today';
    $data['notifications'][] = '<i class="fa fa-users text-aqua"></i> 2 new members joined today';
    $data['notifications'][] = '<i class="fa fa-users text-aqua"></i> 1 new members joined today';
}

view('home', $data);

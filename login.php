<?php
//Login form by Kik
require_once __DIR__ . '/include.php';

$data = [];

if (get_logged_in_user()) {
    redirect('index.php');
}

if (!empty($_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $users = UsersQuery::create()
        ->filterByUsername($username)
        ->limit(1)
        ->find();
    
    if (sizeof($users) == 0) {
        flash_tits('User does not exist', 'danger');
    } else if (!password_verify($password, $users[0]->getPassword())) {
        flash_tits('Wrong password', 'danger');
    } else {
        $_SESSION['user_id'] = $users[0]->getId();

        flash_tits('Logged in successfully', 'success');
        redirect('index.php');
    }
}

view('login', $data);

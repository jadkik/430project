<?php
//logout to redirect to home page and remove the session of the user
//By Nappa
require_once __DIR__ . '/include.php';

unset($_SESSION['user_id']);
redirect('index.php');

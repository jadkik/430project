<?php
//Simple PHP file to query the attendance of trainers, restricted to manager
//By Kik
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('manager');
if (empty($user)) {
    redirect('login.php');
}

$data = [];
/*Bug fix, not member_id, trainer_id used instead
By Nappa 1/5/2016*/
if (isset($_GET['trainer_id'])) {
    $trainer_id = $_GET['trainer_id'];
} else {
    $trainer_id = null;
}

$trainers = TrainersQuery::create()->find();
$data['trainers'] = $trainers;

if (!empty($trainer_id)) {
    $attendances = TrainerAttendanceQuery::create()
        ->filterByTrainerId($trainer_id)
        ->orderByCreatedAt('desc')
        ->find();

    $data['attendances'] = $attendances;

    $interval = DateInterval::createFromDateString('1 day');
    $month = date('Y-m');
    $daily_attendance = array();
    foreach ($attendances as $row) {
        $row_month = $row->getCreatedAt()->format('Y-m');
        $row_day = $row->getCreatedAt()->format('d');
        
        if ($row_month !== $month) {
            continue;
        }
        
        $daily_attendance[$row_day][$row->getAction()] = $row->getCreatedAt();
    }

    $start = new DateTime($month . '-1');
    $end = new DateTime($month . '-1');
    $end->add(new DateInterval('P1M'));
    $period = new DatePeriod($start, $interval, $end);
    $total_hours = 0;
//Force the trainer to checkout, if he does not check out, attendance will not be calculated
//Solution by kik to prevent the trainer from staying checked in always!
    foreach ( $period as $dt ) {
        if ($dt->format('Y-m') !== $month) {
            break;
        }
        $day = $dt->format('d');
        if (isset($daily_attendance[$day])) {
            $checkin = $daily_attendance[$day]['signin'];
            $checkout = $daily_attendance[$day]['signout'];
            if (empty($checkin) && empty($checkout)) {
                continue;
            } else if (empty($checkin)) {
                continue;
            } else if (empty($checkout)) {
                continue;
            } else {
                $total_hours += $checkin->diff($checkout)->h;
            }
        }
    }
    $data['total_hours'] = $total_hours;
    
    $trainer = TrainersQuery::create()->findPk($trainer_id);
    $data['trainer'] = $trainer;
    $data['trainer_id'] = $trainer_id;
}

view('manager_attendance', $data);

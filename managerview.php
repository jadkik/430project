<?php
//Modest query that fills an associative array with the data to be used in datatables in the managerview
//By Nappa
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('manager');
if (empty($user)) {
    redirect('login.php');
}

$con = mysqli_connect("localhost", "root", "root","clubsys");
if (mysqli_connect_errno()) {
    die("Error: " . mysqli_error());
}

$data = [];

$result = mysqli_query($con,"SELECT users.username, members.* FROM members JOIN users ON users.id  = members.id"); 
$result2 =mysqli_query($con,"SELECT users.username, trainers.* FROM trainers JOIN users ON users.id  = trainers.id");
$result3 =mysqli_query($con,"SELECT * FROM facilities");
$result4 = mysqli_query($con,"SELECT trainers.id, courses.* FROM courses JOIN trainers ON trainers.id  = courses.trainer_id"); 
$result5 = mysqli_query($con,"SELECT * FROM inventory"); 

$data['result'] = $result;
$data['result2']=$result2;
$data['result3']=$result3;
$data['result4']=$result4;
$data['result5']=$result5;
view('managerview', $data);

<?php
/*Regular Query to build a listbox of courses registered by a user
	By Nappppaaaaa*/
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$attendances = MemberAttendanceQuery::create()
    ->filterByMemberId($user->getId())
    ->joinWith('MemberAttendance.Courses')
    ->orderByCreatedAt('desc')
    ->find();

$data['attendances'] = $attendances;

$registrations = CourseMemberRegistrationQuery::create()
    ->filterByMemberId($user->getId())
    ->joinWith('CourseMemberRegistration.Courses')
    ->orderByCreatedAt()
    ->find();

$registered_courses = array();
foreach ($registrations as $r) {
    $registered_courses[] = $r->getCourses();
}

$data['registered_courses'] = $registered_courses;

view('member_attendance', $data);

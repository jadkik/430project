<?php
//Query to populate the courses taken by a member
//By Nappa
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$attendances = MemberAttendanceQuery::create()
    ->filterByMemberId($user->getId())
    ->joinWith('MemberAttendance.Courses')
    ->orderByCreatedAt('desc')
    ->find();

$data['attendances'] = $attendances;

$registrations = CourseMemberRegistrationQuery::create()
    ->filterByMemberId($user->getId())
    ->joinWith('CourseMemberRegistration.Courses')
    ->orderByCreatedAt()
    ->find();

$courses = CoursesQuery::create()
    ->leftJoin('Courses.CourseMemberRegistration')
    ->addJoinCondition('CourseMemberRegistration', 'CourseMemberRegistration.MemberId = ?', $user->getId())
    ->withColumn('CourseMemberRegistration.MemberId')
    ->find();

	//Kik modification: loop going out of bounds: fixed!
$registered_courses = array();
foreach ($registrations as $r) {
    $registered_courses[] = $r->getCourses();
}

$data['all_courses'] = $courses;
$data['registered_courses'] = $registered_courses;

view('member_courses', $data);

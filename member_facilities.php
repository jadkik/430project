<?php
//Reserved facilities by a member query
//By Nappa
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$facilities = FacilitiesQuery::create()->find();

$data['facilities'] = $facilities;

$reservations = ReservationsQuery::create()
    ->filterByMemberId($user->getId())
    ->joinWith('Reservations.Facilities')
    ->orderByStartTime('desc')
    ->find();

$data['reservations'] = $reservations;

view('member_facilities', $data);

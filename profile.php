<?php
//Query to modify a profile
//Renewing plan, changing name/passwords
//By Kik
require_once __DIR__ . '/include.php';

$data = [];

$user = get_logged_in_user();
if (empty($user)) {
    redirect('index.php');
}

if ($user->getRole() === 'member') {
    $member = MembersQuery::create()->findPk($user->getId());
    
    

    if (!empty($_POST)) {
        
        switch ($_POST['which']) {
            case 'renew_plan':
                $plan = $_POST['plan'];
                
                $all_plans = array('basic', 'deluxe_monthly', 'deluxe_yearly');
                
                if (!in_array($plan, $all_plans)) {
                    flash_tits('Please select a plan.', 'warning');
                } else {
                    
                    $today = date('Y-m-d');
                    switch ($plan) {
                        case 'basic':
                        case 'deluxe_monthly':
                            $plan_expiry = date('Y-m-d', strtotime("+1 months", strtotime($today)));
                            break;
                        case 'deluxe_yearly':
                            $plan_expiry = date('Y-m-d', strtotime("+1 years", strtotime($today)));
                            break;
                    }
                    
                    $member->setPlan($plan);
                    $member->setPlanExpiry($plan_expiry);
                    $member->save();
                }
                
                break;
            case 'password_update':
                $old_password = $_POST['old_password'];
                $password = $_POST['password'];
                $confirm_password = $_POST['confirm_password'];
                
                if ($password !== $confirm_password) {
                    flash_tits('Passwords do not match.', 'warning');
                } else if (!password_verify($old_password, $user->getPassword())) {
                    flash_tits('Wrong password', 'danger');
                } else {
                    $user->setPassword(password_hash($password, PASSWORD_DEFAULT));
                    $user->save();
                }
                
                break;
            case 'personal_info':
                $username = $_POST['username'];
                $first_name = $_POST['first_name'];
                $last_name = $_POST['last_name'];
                
                $user->setUsername($username);
                $member->setFirstName($first_name);
                $member->setLastName($last_name);
                $user->save();
                $member->save();
                
                break;
        }
        // Clear the POST data
        redirect('profile.php');
        die();
    }

    $data['first_name'] = $member->getFirstName();
    $data['last_name'] = $member->getLastName();
    $data['username'] = $user->getUsername();
    $data['plan_name'] = $member->getPlan();
    $data['plan_expiry'] = $member->getPlanExpiry();
    $data['plan_description'] = $member->getPlanDescription();
//Executes the view after submission, added by Nappa
    view('profile', $data);
} else {
    // TODO: Implement profile for all roles
    redirect('index.php');
}

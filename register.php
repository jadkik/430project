<?php
//Register form to add a new member to the db, note that this never adds a trainer, just members!
//Kik and Nappa
require_once __DIR__ . '/include.php';

$data = [];

if (get_logged_in_user()) {
    redirect('index.php');
}

if (!empty($_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $plan = $_POST['plan'];
    
    $all_plans = array('basic', 'deluxe_monthly', 'deluxe_yearly');
    
    if ($password !== $confirm_password) {
        flash_tits('Passwords do not match.', 'warning');
    } else if (!in_array($plan, $all_plans)) {
        flash_tits('Please select a plan.', 'warning');
    } else {
        $user = new Users();
        $user->setUsername($username);
        $user->setPassword(password_hash($password, PASSWORD_DEFAULT));
        $user->setRole('member');
        $user->save();
        
        $today = date('Y-m-d');
		//Included plans for the member on registration, no payment though
		//Kik
        switch ($plan) {
            case 'basic':
            case 'deluxe_monthly':
                $plan_expiry = date('Y-m-d', strtotime("+1 months", strtotime($today)));
                break;
            case 'deluxe_yearly':
                $plan_expiry = date('Y-m-d', strtotime("+1 years", strtotime($today)));
                break;
        }
        
        $member = new Members();
        $member->setId($user->getId());
        $member->setFirstName($first_name);
        $member->setLastName($last_name);
        $member->setPlan($plan);
        $member->setPlanExpiry($plan_expiry);
        $member->save();
        
        $_SESSION['user_id'] = $user->getId();
        redirect('index.php');
    }
}

view('register', $data);

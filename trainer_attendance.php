<?php
//Trainer viewing his attendance, almost the same as member_attendance
//Copied and modified by Nappa
require_once __DIR__ . '/include.php';

$user = get_logged_in_user('trainer');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$attendances = TrainerAttendanceQuery::create()
    ->filterByTrainerId($user->getId())
    ->orderByCreatedAt('desc')
    ->find();

$data['attendances'] = $attendances;

$interval = DateInterval::createFromDateString('1 day');
$month = date('Y-m');
$daily_attendance = array();
foreach ($attendances as $row) {
    $row_month = $row->getCreatedAt()->format('Y-m');
    $row_day = $row->getCreatedAt()->format('d');
    
    if ($row_month !== $month) {
        continue;
    }
    
    $daily_attendance[$row_day][$row->getAction()] = $row->getCreatedAt();
}

$start = new DateTime($month . '-1');
$end = new DateTime($month . '-1');
$end->add(new DateInterval('P1M'));
$period = new DatePeriod($start, $interval, $end);
$total_hours = 0;

foreach ( $period as $dt ) {
    if ($dt->format('Y-m') !== $month) {
        break;
    }
    $day = $dt->format('d');
    if (isset($daily_attendance[$day])) {
        $checkin = $daily_attendance[$day]['signin'];
        $checkout = $daily_attendance[$day]['signout'];
        if (empty($checkin) && empty($checkout)) {
            continue;
        } else if (empty($checkin)) {
            continue;
        } else if (empty($checkout)) {
            continue;
        } else {
            $total_hours += $checkin->diff($checkout)->h;
        }
    }
}
$data['total_hours'] = $total_hours;

view('trainer_attendance', $data);

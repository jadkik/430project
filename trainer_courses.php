<?php
//Courses governed by the trainer, simple query building

require_once __DIR__ . '/include.php';

$user = get_logged_in_user('trainer');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

if (isset($_GET['member_id'])) {
    $member_id = $_GET['member_id'];
} else {
    $member_id = null;
}

$members = MembersQuery::create()
    ->joinWith('Members.CourseMemberRegistration')
    ->joinWith('CourseMemberRegistration.Courses')
    ->where('Courses.TrainerId = ?', $user->getId())
    ->orderByFirstName()
    ->orderByLastName()
    ->find();

$data['members'] = $members;
$data['member_id'] = $member_id;

if (!empty($member_id)) {
    $attendances = MemberAttendanceQuery::create()
        ->filterByMemberId($member_id)
        ->joinWith('MemberAttendance.Courses')
        ->where('Courses.TrainerId = ?', $user->getId())
        ->orderByCreatedAt('desc')
        ->find();

    $member = MembersQuery::create()->findPk($member_id);

    $data['attendances'] = $attendances;
    $data['member'] = $member;
} else {
    $data['attendances'] = array();
}

view('trainer_courses', $data);

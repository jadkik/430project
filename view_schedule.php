<?php

require_once __DIR__ . '/include.php';

$user = get_logged_in_user('member');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$events = array();

$query = ScheduleQuery::create();

$today = date('Y-m-d');
$oneweekago = date('Y-m-d', strtotime("-1 weeks", strtotime($today)));
$twomonthslater = date('Y-m-d', strtotime("+2 months", strtotime($today)));

$query = $query->filterByStartDate(array('max' => $twomonthslater));
$query = $query->filterByEndDate(array('min' => $oneweekago));
$query = $query->filterByMemberId($user->getId());

$interval = DateInterval::createFromDateString('1 day');

$weekday_index = str_split('MTWRFSU');
$events = array();

foreach ($query->find() as $row) {
    $time_start = $row->getStartTime();
    $time_end = $row->getEndTime();
    $weekdays = $row->getWeekdays();
    
    $start_time_str = $time_start->format('H:i:s');
    $end_time_str = $time_end->format('H:i:s');
    
    $start = $row->getStartDate();
    $end = $row->getEndDate();
    $period = new DatePeriod($start, $interval, $end);
    
    $color = '#' . substr(md5('schedule' . $row->getId()), 0, 6);
    $inverted = color_inverse($color);
    
    foreach ( $period as $dt ) {
        $weekday = $dt->format("w") * 1;
        if (strpos($weekdays, $weekday_index[$weekday]) === FALSE) {
            continue;
        }
        
        $events[] = array(
            "title" => 'Course - ' . $row->getTitle(),
            "start" => $dt->format("Y-m-d") . " " . $start_time_str,
            "end" => $dt->format("Y-m-d") . " " . $start_time_str,
            "allDay" => false,
            "backgroundColor" => $color,
            "borderColor" => $color,
            "textColor" => $inverted,
        );
    }
}

$query2 = new ReservationsQuery();
$query2 = $query2->filterByStartTime(array('max' => $twomonthslater));
$query2 = $query2->filterByEndTime(array('min' => $oneweekago));
$query2 = $query2->filterByMemberId($user->getId());

foreach ($query2->find() as $row) {
    $start = $row->getStartTime();
    $end = $row->getEndTime();
    $fac = $row->getFacilityId();
    $facility = (new FacilitiesQuery())->findPk($fac);
    $name = $facility->getName();
    
    $color = '#' . substr(md5('reservation' . $row->getId()), 0, 6);
    $inverted = color_inverse($color);
    
    $events[] = array(
        "title" => 'Facility - ' . $name,
        "start" => $start->format("Y-m-d H:i:s"),
        "end" => $end->format("Y-m-d H:i:s"),
        "allDay" => false,
        "backgroundColor" => $color,
        "borderColor" => $color,
        "textColor" => $inverted,
    );
}

$data['events'] = $events;

view('schedule', $data);

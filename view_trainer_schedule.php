<?php

require_once __DIR__ . '/include.php';
//By Kik
$user = get_logged_in_user('trainer');
if (empty($user)) {
    redirect('login.php');
}

$data = [];

$events = array();

$query = new CoursesQuery();

$today = date('Y-m-d');
$oneweekago = date('Y-m-d', strtotime("-1 weeks", strtotime($today)));
$twomonthslater = date('Y-m-d', strtotime("+2 months", strtotime($today)));
$start = new DateTime($oneweekago);
$end = new DateTime($twomonthslater);

$query = $query->filterByTrainerId($user->getId());

$interval = DateInterval::createFromDateString('1 day');

$weekday_index = str_split('MTWRFSU');
$events = array();

foreach ($query->find() as $row) {
    $time_start = $row->getStartTime();
    $time_end = $row->getEndTime();
    $weekdays = $row->getWeekdays();
    
    $start_time_str = $time_start->format('H:i:s');
    $end_time_str = $time_end->format('H:i:s');
    
    $period = new DatePeriod($start, $interval, $end);
    
    $color = '#' . substr(md5('course' . $row->getId()), 0, 6);
    $inverted = color_inverse($color);
    
    foreach ( $period as $dt ) {
        $weekday = $dt->format("w") * 1;
        if (strpos($weekdays, $weekday_index[$weekday]) === FALSE) {
            continue;
        }
        
        $events[] = array(
            "title" => 'Course - ' . $row->getName(),
            "start" => $dt->format("Y-m-d") . " " . $start_time_str,
            "end" => $dt->format("Y-m-d") . " " . $start_time_str,
            "allDay" => false,
            "backgroundColor" => $color,
            "borderColor" => $color,
            "textColor" => $inverted,
        );
    }
}

$data['events'] = $events;

view('schedule', $data);

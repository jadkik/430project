  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2016 <a href="/"><strong>Club</strong>SEEST&Egrave;M</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->
<script>
  if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
          position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.indexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
  }
  function updateNav() {
    $("section.sidebar li.active").removeClass("active");
    $("section.sidebar a[href]").each(function(i, el) {
      var href = document.location.href;
      var hash = document.location.hash;
      console.log(href, hash, $(el).attr("href"));
      if (href.endsWith("/")) {
        href = href + "index.php";
      }
      if (href.endsWith($(el).attr("href"))) {
        $(el).parents("li").addClass("active");
        console.log($(el).parents("li"));
      }
    });
  }
  $(document).on("ready", updateNav);
  $(window).on("hashchange", updateNav);
</script>
</body>
</html>

<ul class="sidebar-menu">
    <!-- about contact view-courses view-facilities -->
    <li class="header">WELCOME</li>
    <li>
      <a href="index.php">
        <i class="fa fa-home"></i> <span>Home</span>
      </a>
    </li>
    <li>
      <a href="about.php">
        <i class="fa fa-info-circle"></i> <span>About</span>
      </a>
    </li>
    <li>
      <a href="about.php#contact">
        <i class="fa fa-comments-o"></i> <span>Contact</span>
      </a>
    </li>
    <li class="header">CLUB OFFERINGS</li>
    <li>
      <a href="view_courses.php">
        <i class="fa fa-graduation-cap"></i> <span>Courses</span>
      </a>
    </li>
    <li>
      <a href="view_facilities.php">
        <i class="fa fa-rocket"></i> <span>Facilities</span>
      </a>
    </li>
</ul>

<ul class="sidebar-menu">
    <!-- about contact view-courses view-facilities -->
    <li class="header">WELCOME</li>
    <li>
      <a href="index.php">
        <i class="fa fa-home"></i> <span>Home</span>
      </a>
    </li>
    <li>
      <a href="about.php">
        <i class="fa fa-info-circle"></i> <span>About</span>
      </a>
    </li>
    <li>
      <a href="about.php#contact">
        <i class="fa fa-comments-o"></i> <span>Contact</span>
      </a>
    </li>
    <li class="header">CLUB OFFERINGS</li>
    <li>
      <a href="view_courses.php">
        <i class="fa fa-graduation-cap"></i> <span>Courses</span>
      </a>
    </li>
    <li>
      <a href="view_facilities.php">
        <i class="fa fa-rocket"></i> <span>Facilities</span>
      </a>
    </li>
    
    <!-- managerview[members,trainers,facilities,courses]  -->
    <li class="header">MANAGEMENT</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-users"></i>
        <span>Users</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="managerview.php#!/members"><i class="fa fa-circle-o"></i> Members</a></li>
        <li><a href="managerview.php#!/trainers"><i class="fa fa-circle-o"></i> Trainers</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-briefcase"></i>
        <span>Operations</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="managerview.php#!/facilities"><i class="fa fa-circle-o"></i> Facilities</a></li>
        <li><a href="managerview.php#!/courses"><i class="fa fa-circle-o"></i> Courses</a></li>
      </ul>
    </li>
    <li>
      <a href="trainer_attendance.php">
        <i class="fa fa-bullhorn"></i> <span>Attendance</span>
      </a>
    </li>
    <li>
      <a href="managerview.php#!/inventory">
        <i class="fa fa-cubes"></i> <span>Inventory</span>
      </a>
    </li>
    <li>
      <a href="manager_attendance.php">
        <i class="fa fa-cubes"></i> <span>Trainers' attendance</span>
      </a>
    </li>
</ul>


    <!-- Content Header (Page header) -->
<div class="box collapsed-box" id="courses-box">
        <div class="box-header with-border">
          <h3 class="box-title">Courses</h3>
		  
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">
			<table id="datatables4" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
							<th></th>
							<th>ID</th>
							<th>Course Name</th>
							<th>Trainer</th>
							<th>Description</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Deluxe</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($result3)) {
						?>
						<tr>
							<td></td>
							<td><?=$row['name'] ?></td>
							<td><?=$row['trainer_id']?></td>
							<td><?=$row['description']?></td>
							<td><?=$row['start_time']?></td>
							<td><?=$row['end_time']?></td>
							<td><?=$row['is_deluxe']?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
  </div>
  <!-- /.box -->

    
  <script type="text/javascript" charset="utf-8">
	var editor4; // use a global for the submit and return data rendering in the examples
	 
	$(document).on("ready", function() {
		editor4 = new $.fn.dataTable.Editor( {
			ajax: "ajax/courses.php",
			table: "#datatables4",
			fields: [
				{
					label: "Name:",
					name: "courses.name"
				},
				{
					label: "Trainer:",
					name: "courses.trainer_id",
					type: "select",
					placeholder: "Select a trainer"
				}, 
				{
					label: "Description:",
					name: "courses.description"
				}, {
					label: "Start Time:",
					name: "courses.start_time",
					type: "datetime",
					format:  'h:mm a',
				}, {
					label: "End Time:",
					name: "courses.end_time",
					type: "datetime",
					format:  'h:mm a'
				}, 
				 {
					label: "Deluxe:",
					name: "courses.is_deluxe",
					type:"select",
					options: [
                    { label: "Yes", value: "1" },
                    { label: "No",   value: "0" }
					]
				}
			]
		} );
	 
	   
		$('#datatables4').on( 'click', 'tbody td:not(:first-child)', function (e) {
			editor4.inline(this, {
				onBlur: 'submit',
			});
		} );
	 
		$('#datatables4').DataTable( {
			dom: "Bfrtip",
			ajax: "ajax/courses.php",
			columns: [
				{
					data: null,
					defaultContent: '',
					className: 'select-checkbox',
					orderable: false
				},
				{
					data:"courses.id"
				},
				{ data: "courses.name" },
				{ data: "courses.trainer_id" , render: {"display": function( data, type, row, meta )  {
					if(row.trainers){
					return row.trainers.first_name +" "+row.trainers.last_name;
				}else{
					return ' ';
				}
				}}},
				{ data: "courses.description" },
				{ data: "courses.start_time" },
				{ data: "courses.end_time" },
				{data:"courses.is_deluxe", render: {"display": function( data, type, row, meta )  
				{
					if(data == 1)
					{
						return "Yes";
					}
					else
					{
						return "No";
					}
					
				}
				}}
			],
			select: {
				style:    'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "create", editor: editor4 },
				{ extend: "edit",   editor: editor4 },
				{ extend: "remove", editor: editor4 }
			]
		} );
	} );
</script>


    <!-- Content Header (Page header) -->
<div class="box collapsed-box" id="facilities-box">
        <div class="box-header with-border">
          <h3 class="box-title">Facilities</h3>
		  
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">
			<table id="datatables3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
							<th></th>
							<th>ID</th>
							<th>Facility Name</th>
							<th>Capacity</th>
							<th>Description</th>
							<th>Opening Time</th>
							<th>Closing Time</th>
							<th>Deluxe</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($result3)) {
						?>
						<tr>
							<td></td>
							<td><?=$row['name']?></td>
							<td><?=$row['capacity']?></td>
							<td><?=$row['description']?></td>
							<td><?=$row['opening_time']?></td>
							<td><?=$row['closing_time']?></td>
							<td><?=$row['is_deluxe']?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
  </div>
  <!-- /.box -->

    
  <script type="text/javascript" charset="utf-8">
	var editor3; // use a global for the submit and return data rendering in the examples
	 
	$(document).on("ready", function() {
		editor3 = new $.fn.dataTable.Editor( {
			ajax: "ajax/facilities.php",
			table: "#datatables3",
			fields: [
				{
					label: "Name:",
					name: "facilities.name"
				}, {
					label: "Capacity:",
					name: "facilities.capacity"
				}, {
					label: "Description:",
					name: "facilities.description"
				}, {
					label: "Opening Time:",
					name: "facilities.opening_time",
					type: "datetime",
					format:  'h:mm a',
				}, {
					label: "Closing Time:",
					name: "facilities.closing_time",
					type: "datetime",
					format:  'h:mm a'
				}, 
				 {
					label: "Deluxe:",
					name: "facilities.is_deluxe",
					type:"select",
					options: [
                    { label: "Yes", value: "1" },
                    { label: "No",   value: "0" }
					]
				}
			]
		} );
	 
	   
		$('#datatables3').on( 'click', 'tbody td:not(:first-child)', function (e) {
			editor3.inline(this, {
				onBlur: 'submit',
			});
		} );
	 
		$('#datatables3').DataTable( {
			dom: "Bfrtip",
			ajax: "ajax/facilities.php",
			columns: [
				{
					data: null,
					defaultContent: '',
					className: 'select-checkbox',
					orderable: false
				},
				{
					data:"facilities.id"
				},
				{ data: "facilities.name" },
				{ data: "facilities.capacity" },
				{ data: "facilities.description" },
				{ data: "facilities.opening_time" },
				{ data: "facilities.closing_time" },
				{data:"facilities.is_deluxe", render: {"display": function( data, type, row, meta )  
				{
					if(data == 1)
					{
						return "Yes";
					}
					else
					{
						return "No";
					}
					
				}
				}}
			],
			select: {
				style:    'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "create", editor: editor3 },
				{ extend: "edit",   editor: editor3 },
				{ extend: "remove", editor: editor3 }
			]
		} );
	} );
</script>

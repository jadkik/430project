<?php include 'base_head.php'; ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="jumbotron">
          <h1>Welcome to ClubSEEST&Egrave;M!</h1>
          <?php if (empty($user)): ?>
          <p>Start taking courses in everything from Pilates to Powerlifting in two easy steps!</p>
          <p><a class="btn btn-primary btn-lg" href="register.php" role="button">Learn more</a></p>
          <?php elseif ($user->getRole() == 'member'): ?>
          <p>View the list of available courses</p>
          <p>
            <a class="btn btn-primary btn-lg" href="member_courses.php" role="button">View courses</a>
			<!-- EDITED BY ALI, buttons referred to the static pages, changed to the active session pages!-->
            <a class="btn btn-primary btn-lg" href="view_schedule.php" role="button">View schedule</a>
			<a class="btn btn-primary btn-lg" href="member_facilities.php" role="button">Reserve facilities</a>
          </p>
          <?php elseif ($user->getRole() == 'manager'): ?>
          <p>Manage users and operations!</p>
          <p>
            <a class="btn btn-primary btn-lg" href="managerview.php#!/members" role="button">View members</a>
            <a class="btn btn-primary btn-lg" href="managerview.php#!/facilities" role="button">View facilities</a>
          </p>
          <?php elseif ($user->getRole() == 'trainer'): ?>
          <p>Check your courses schedule!</p>
          <p><a class="btn btn-primary btn-lg" href="view_trainer_schedule.php" role="button">View schedule</a></p>
          <?php endif; ?>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="container">
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/pilates.jpg" alt="Pilates">
            <div class="caption">
              <h3>Pilates</h3>
              <p>
                Enjoy one of our relaxing Pilates courses.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/Powerlifting.jpg" alt="Powerlifting">
            <div class="caption">
              <h3>Powerlifting</h3>
              <p>
                Train for powerlifting and become a beast with our best instructors.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/bjj-jjj.jpg" alt="Jiu Jitsu">
            <div class="caption">
              <h3>Jiu Jitsu</h3>
              <p>
                Become a master in Jiu Jitsu, Japanese or Brazilian.<br />
                Master the art of fighting in a year.
              </p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <p><a href="view_courses.php" class="btn btn-primary" role="button">View all</a></p>
      </div>

    </section>
    <!-- /.content -->

<?php include 'base_foot.php'; ?>
			<!--Last modified by kik, included bootstrap for the buttons-->

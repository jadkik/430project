
    <!-- Content Header (Page header) -->
<div class="box collapsed-box" id="inventory-box">
        <div class="box-header with-border">
          <h3 class="box-title">Inventory</h3>
		  
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">
			<table id="datatables5" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
							<th></th>
							<th>ID</th>
							<th>Item Name</th>
							<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($result3)) {
						?>
						<tr>
							<td></td>
							<td><?=$row['id']?></td>
							<td><?=$row['name']?></td>
							<td><?=$row['status']?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
  </div>
  <!-- /.box -->

    
  <script type="text/javascript" charset="utf-8">
	var editor5; // use a global for the submit and return data rendering in the examples
	 
	$(document).on("ready", function() {
		editor5 = new $.fn.dataTable.Editor( {
			ajax: "ajax/inventory.php",
			table: "#datatables5",
			fields: [
				{
					label: "Name:",
					name: "inventory.name"
				}, {
					label: "Status:",
					name: "inventory.status"
				}
			]
		} );
	 
	   
		$('#datatables5').on( 'click', 'tbody td:not(:first-child)', function (e) {
			editor5.inline(this, {
				onBlur: 'submit',
			});
		} );
	 
		$('#datatables5').DataTable( {
			dom: "Bfrtip",
			ajax: "ajax/inventory.php",
			columns: [
				{
					data: null,
					defaultContent: '',
					className: 'select-checkbox',
					orderable: false
				},
				{
					data:"inventory.id"
				},
				{ data: "inventory.name" },
				{ data: "inventory.status" }
			],
			select: {
				style:    'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "create", editor: editor5 },
				{ extend: "edit",   editor: editor5 },
				{ extend: "remove", editor: editor5 }
			]
		} );
	} );
</script>

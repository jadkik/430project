<?php include 'base_head.php'; ?>
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
          <div id="message-container"></div>
      </div>
  </div>
  <div class="row">
    
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-body">
          <form action="" method="get">
                <div class="form-group">
                  <label for="exampleInputEmail1">Member</label>
                  <select class="form-control" name="trainer_id" id="exampleInputEmail1">
                      <option value="">Choose a trainer...</option>
                      <?php foreach ($trainers as $row): ?>
                      <option value="<?=$row->getId()?>"><?=$row->getFirstName()?> <?=$row->getLastName()?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">View</button>
                </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
    
    <?php if (!empty($trainer_id)): ?>
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <table id="datatables3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Date/Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($attendances as $row) {
                    ?>
                    <tr>
                        <td><?=$row->getAction()?></td>
                        <td><?=$row->getCreatedAt()->format('Y-m-d H:i:s')?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Viewing: <?=$trainer->getFirstName()?> <?=$trainer->getLastName()?></th>
                </tr>
                <tr>
                    <th colspan="2">Total hours: <?=$total_hours?></th>
                </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
    <?php endif; ?>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
<?php include 'base_foot.php'; ?>

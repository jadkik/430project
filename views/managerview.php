<?php include 'base_head.php'; ?>
<!-- Main content -->
    <section class="content">
<?php include 'members.php'; ?>
<?php include 'trainers.php'; ?>
<?php include 'facilities.php';?>
<?php include 'courses.php';?>
<?php include 'inventory.php';?>
</section>
<script>
    function potato() {
        $("#courses-box:not(.collapsed-box),#trainers-box:not(.collapsed-box),#facilities-box:not(.collapsed-box),#members-box:not(.collapsed-box)").toggleBox();
        var hash = document.location.hash.substring(3);
        if (hash == "courses") {
            $("#courses-box").toggleBox();
        } else if (hash == "trainers") {
            $("#trainers-box").toggleBox();
        } else if (hash == "facilities") {
            $("#facilities-box").toggleBox();
        } else if (hash == "members") {
            $("#members-box").toggleBox();
        } else if (hash == "inventory") {
            $("#inventory-box").toggleBox();
        }
    }
    $(window).on('hashchange', potato);
    $(document).on('ready', potato);
</script>
<?php include 'base_foot.php'; ?>

<?php include 'base_head.php'; ?>
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
          <div id="message-container"></div>
      </div>
  </div>
  <div class="row">
    
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-body">
          <form id="checkinForm">
                <input type="hidden" name="action" value="signin">
                <div class="form-group">
                  <label for="exampleInputEmail1">Course</label>
                  <select class="form-control" name="course_id" id="exampleInputEmail1">
                      <?php foreach ($registered_courses as $row): ?>
                      <option value="<?=$row->getId()?>"><?=$row->getName()?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <button type="button" class="btn btn-primary" id="doCheckin">Check in</button>
                </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
    
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <table id="datatables3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Course Name</th>
                    <th>Action</th>
                    <th>Date/Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($attendances as $row) {
                    ?>
                    <tr>
                        <td><?=$row->getCourses()->getName()?></td>
                        <td><?=$row->getAction()?></td>
                        <td><?=$row->getCreatedAt()->format('Y-m-d H:i:s')?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
<script>
  $(function () {
      $("#doCheckin").on('click', function() {
          var $taht = $(this);
          if ($taht.hasClass("disabled")) {
              return false;
          }
          $taht.addClass("disabled");
          $.ajax({
              url: 'ajax/checkin.php',
              data: $("#checkinForm").serialize(),
              method: "POST",
              dataType: "json",
          }).always(function() {
              $taht.removeClass("disabled");
          }).success(function(data) {
              if (data.success) {
                window.location.reload();
              } else {
                  var $d = $('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <strong><i class="icon fa fa-ban"></i> Error</strong>&nbsp;<span></span></div>');
                  $d.find('span').text(data.message);
                  $("#message-container").append($d);
              }
          });
          return false;
      });
  });
</script>
<?php include 'base_foot.php'; ?>

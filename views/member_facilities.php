<?php include 'base_head.php'; ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <table id="datatables3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Capacity</th>
                    <th>Availability</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($facilities as $row): ?>
                <tr>
                  <td><?=$row->getName()?></td>
                  <td><?=$row->getDescription()?></td>
                  <td><?=$row->getCapacity()?></td>
                  <td><?=$row->getOpeningTime()->format('H:i')?>&nbsp;-&nbsp;<?=$row->getClosingTime()->format('H:i')?></td>
                  <td>
                    <button type="button" class="btn btn-default row-reserve-facility" data-facility-id="<?=$row->getId()?>" data-facility-name="<?=$row->getName()?>">Reserve</button>
                  </td>
                </tr>
                <?php
                endforeach;
                if (count($facilities) == 0) {
                  echo '<tr><td colspan="4">No data to show.</td></tr>';
                }
                ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
</section>
<!-- /.content -->
<div class="modal" id="reserve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reserve a facility</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <div id="message-container"></div>
        </div>
        <form id="reserveForm">
          <input type="hidden" name="facility_id">
          <fieldset>
            <legend>Reserve facility: <span class="facility-name"></span></legend>
            
            <div class="form-group">
              <label>Date:</label>

              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control pull-right datepicker" type="text" name="date">
              </div>
              <!-- /.input group -->
            </div>
            
            <div class="bootstrap-timepicker">
              <div class="form-group">
                <label>From:</label>

                <div class="input-group">
                  <input class="form-control timepicker" type="text" name="from_time">

                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                </div>
                <!-- /.input group -->
              </div>
            </div>
            
            <div class="bootstrap-timepicker">
              <div class="form-group">
                <label>To:</label>

                <div class="input-group">
                  <input class="form-control timepicker" type="text" name="to_time">

                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                </div>
                <!-- /.input group -->
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="doReserve">Reserve Now</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
  $(function () {
    
      $(".row-reserve-facility").on('click', function() {
          var $taht = $(this);
          var facility_id = $taht.data('facility-id');
          var facility_name = $taht.data('facility-name');
          $("#reserveForm .facility-name").text(facility_name);
          $("#reserveForm [name=facility_id]").val(facility_id);
          $("#message-container").html("");
          $("#reserve-modal").modal();
          return false;
      });
    
      $("#doReserve").on('click', function() {
          var $taht = $(this);
          if ($taht.hasClass("disabled")) {
              return false;
          }
          $taht.addClass("disabled");
          $.ajax({
              url: 'ajax/facility_reserve.php',
              data: $("#reserveForm").serialize(),
              method: "POST",
              dataType: "json",
          }).always(function() {
              $taht.removeClass("disabled");
          }).success(function(data) {
              if (data.success) {
                window.location.reload();
              } else {
                  var $d = $('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <strong><i class="icon fa fa-ban"></i> Error</strong>&nbsp;<span></span></div>');
                  $d.find('span').text(data.message);
                  $("#message-container").append($d);
              }
          });
          return false;
      });
      
      //Date picker
      $('.datepicker').datepicker({
        autoclose: true
      });
      
      //Timepicker
      $(".timepicker").timepicker({
        showInputs: false
      });
  });
</script>
<?php include 'base_foot.php'; ?>

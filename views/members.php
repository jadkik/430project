
    <!-- Content Header (Page header) -->
<div class="box collapsed-box" id="members-box">
        <div class="box-header with-border">
          <h3 class="box-title">Members</h3>
		  
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">
			<table id="datatables" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
							<th></th>
							<th>username</th>
							<th>first_name</th>
							<th>last_name</th>
							<th>plan</th>
							<th>plan_expiry</th>
							<th>height</th>
							<th>width</th>
							<th>trainer_id</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($result)) {
						?>
						<tr>
							<td></td>
							<td><?=$row['username']?></td>
							<td><?=$row['first_name']?></td>
							<td><?=$row['last_name']?></td>
							<td><?=$row['plan']?></td>
							<td><?=$row['plan_expiry']?></td>
							<td><?=$row['height']?></td>
							<td><?=$row['weight']?></td>
							<td><?=$row['trainer_id']?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
  </div>
  <!-- /.box -->

    
  <script type="text/javascript" charset="utf-8">
	var editor; // use a global for the submit and return data rendering in the examples
	 
	$(document).on("ready", function() {
		editor = new $.fn.dataTable.Editor( {
			ajax: "ajax/members.php",
			table: "#datatables",
			fields: [
				{
					label: "Username:",
					name: "users.username"
				}, {
					label: "First Name:",
					name: "members.first_name"
				}, {
					label: "Last Name:",
					name: "members.last_name"
				}, {
					label: "Plan:",
					name: "members.plan"
				}, {
					label: "Plan Expires On:",
					name: "members.plan_expiry",
					type: "datetime"
				}, {
					label: "Height:",
					name: "members.height"
				}, 
				 {
					label: "Weight:",
					name: "members.weight"
				},
				{
					label: "Trainer:",
					name: "members.trainer_id",
					type: "select",
					placeholder: "Select a trainer"
				}
			]
		} );
	 
	   
		$('#datatables').on( 'click', 'tbody td:not(:first-child)', function (e) {
			editor.inline(this, {
				onBlur: 'submit',
			});
		} );
	 
		$('#datatables').DataTable( {
			dom: "Bfrtip",
			ajax: "ajax/members.php",
			columns: [
				{
					data: null,
					defaultContent: '',
					className: 'select-checkbox',
					orderable: false
				},
				{ data: "users.username" },
				{ data: "members.first_name" },
				{ data: "members.last_name" },
				{ data: "members.plan" },
				{ data: "members.plan_expiry" },
				{ data: "members.height", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
				{ data: "members.weight", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
				{data:"members.trainer_id", render: {"display": function( data, type, row, meta )  {
					return row.trainers.first_name +" "+row.trainers.last_name;
				}}}
			],
			select: {
				style:    'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "edit",   editor: editor },
				{ extend: "remove", editor: editor }
			]
		} );
	} );
</script>

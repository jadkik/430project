<?php include 'base_head.php'; ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-9">
      <form action="" method="post">
        <input type="hidden" name="which" value="personal_info">
        <div class="box" id="one-box">
          <div class="box-header with-border">
            <h3 class="box-title">Personal information</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group has-feedback">
              <input name="first_name" type="text" class="form-control" placeholder="First name" value="<?=$first_name?>">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input name="last_name" type="text" class="form-control" placeholder="Last name" value="<?=$last_name?>">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input name="username" type="email" class="form-control" placeholder="Email" value="<?=$username?>">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-flat pull-right">Update Info</button>
          </div>
        </div>
      </form>
    </div>
    
    <div class="col-md-3">
      <form action="" method="post">
        <input type="hidden" name="which" value="password_update">
        <div class="box" id="two-box">
          <div class="box-header with-border">
            <h3 class="box-title">Security</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group has-feedback">
              <input name="old_password" type="password" class="form-control" placeholder="Old Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input name="password" type="password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input name="confirm_password" type="password" class="form-control" placeholder="Retype password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-flat pull-right">Change Password</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <form action="" method="post">
        <input type="hidden" name="which" value="renew_plan">
        <div class="box" id="three-box">
          <div class="box-header with-border">
            <h3 class="box-title">Plan</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <select name="plan" class="form-control" placeholder="Select a plan">
                      <option value="">Select a plan</option>
                      <option value="basic" <?=($plan_name=='basic')? 'selected':''?>>Basic (5$)</option>
                      <option value="deluxe_monthly" <?=($plan_name=='deluxe_monthly')? 'selected':''?>>Deluxe - One Month (15$)</option>
                      <option value="deluxe_yearly" <?=($plan_name=='deluxe_yearly')? 'selected':''?>>Delux - One Year (150$)</option>
                  </select>
                </div>
              </div>
              <div class="col-md-8">
                <p>
                  Your current plan is <strong><?=$plan_name?></strong>. It expires <strong>on <?=$plan_expiry->format('Y-m-d')?></strong>.<br /><br />
                  <?=$plan_description?>
                </p>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-flat pull-right">Renew Plan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
<?php include 'base_foot.php'; ?>

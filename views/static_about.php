<?php include 'base_head.php'; ?>
    <!-- Content Header (Page header) -->
    <section class="content-header" id="about">
      <div class="container">
        <div class="jumbotron">
          <h1>ClubSEEST&Egrave;M</h1>
          <p>ClubSEEST&Egrave;M is the leading Gymnasium in the Middle East with its state-of-the-art facilities and the best trainers and instructors internationally.</p>
          <p>By registering with us, you will benefit from all the good things in a gym and a sports club in one package.</p>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content" id="contact">

      <div class="container">
        <div class="jumbotron clearfix">
          <h1>Contact ClubSEEST&Egrave;M</h1>
          <div class="col-sm-6 col-md-5">
            <p>
              You can find us at the following address:
            </p>
            <address>
              <strong>ClubSEEST&Egrave;M</strong><br>
              المعمورة، الضاحيه الجنوبيه<br>
              بيروت، لبنان 12877<br>
            </address>
          </div>
          
          <div class="col-sm-6 col-md-7">
            <p>Contact us at:</p>
            <p>
              <abbr title="Phone">P:</abbr> (961) 1 984-456
            </p>
            <p>
              <abbr title="Email">E:</abbr> admin@clubsys.com
            </p>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->

<?php include 'base_foot.php'; ?>

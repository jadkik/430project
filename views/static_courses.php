<?php include 'base_head.php'; ?>
    <!-- Main content -->
    <section class="content">

      <div class="container">
        <div class="col-sm-12">
          <p>Choose from our selection of courses:</p>
        </div>
      </div>

      <div class="container">
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/pilates.jpg" alt="Pilates">
            <div class="caption">
              <h3>Pilates</h3>
              <p>
                Enjoy one of our relaxing Pilates courses.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/Powerlifting.jpg" alt="Powerlifting">
            <div class="caption">
              <h3>Powerlifting</h3>
              <p>
                Train for powerlifting and become a beast with our best instructors.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/bjj-jjj.jpg" alt="Jiu Jitsu">
            <div class="caption">
              <h3>Jiu Jitsu</h3>
              <p>
                Become a master in Jiu Jitsu, Japanese or Brazilian.<br />
                Master the art of fighting in a year.
              </p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/Cardio-Best-Way-Lose-Weight.jpg" alt="Cardio">
            <div class="caption">
              <h3>Cardio: The best way to lose weight</h3>
              <p>
                Lose weight responsibly with our specialized trainers.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/judo.jpg" alt="Judo">
            <div class="caption">
              <h3>Judo</h3>
              <p>
                Become a professional Judoka in 3 months only! Join our team of trainers in this amazing journey.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/Zumba.jpg" alt="Zumba">
            <div class="caption">
              <h3>Zumba</h3>
              <p>
                Lose weight while dancing and having fun!
              </p>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->

<?php include 'base_foot.php'; ?>

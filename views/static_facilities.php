<?php include 'base_head.php'; ?>
    <!-- Main content -->
    <section class="content">

      <div class="container">
        <div class="col-sm-12">
          <p>Reserve any of the following facilities for as long as you need:</p>
        </div>
      </div>

      <div class="container">
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/basket.png" alt="Pilates">
            <div class="caption">
              <h3>Basketball Court</h3>
              <p>
                Play with friends or alone.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/tennis.jpeg" alt="Powerlifting">
            <div class="caption">
              <h3>Tennis</h3>
              <p>
                Train, learn or play with everyone.
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="dist/img/Pool.jpg" alt="Jiu Jitsu">
            <div class="caption">
              <h3>Swimming Pool</h3>
              <p>
                Reserve a section of the pool for any reason you like.
              </p>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->

<?php include 'base_foot.php'; ?>

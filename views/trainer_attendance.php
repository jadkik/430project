<?php include 'base_head.php'; ?>
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
          <div id="message-container"></div>
      </div>
  </div>
  <div class="row">
    
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-body">
          <form id="checkinForm">
                <input type="hidden" name="action" value="signin">
                <div class="form-group">
                  <button type="button" class="btn btn-primary do-checkin">Check in</button>
                </div>
          </form>
          <form id="checkoutForm">
                <input type="hidden" name="action" value="signout">
                <div class="form-group">
                  <button type="button" class="btn btn-primary do-checkin">Check out</button>
                </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
    
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <table id="datatables3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Date/Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($attendances as $row) {
                    ?>
                    <tr>
                        <td><?=$row->getAction()?></td>
                        <td><?=$row->getCreatedAt()->format('Y-m-d H:i:s')?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="2">
                  Total hours this month: <?=$total_hours?>. Note: If you do not check out, it is not counted in your total hours.
                </th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
<script>
  $(function () {
      $(".do-checkin").on('click', function() {
          var $taht = $(this);
          if ($taht.hasClass("disabled")) {
              return false;
          }
          $taht.addClass("disabled");
          var $form = $taht.parents("form");
          $.ajax({
              url: 'ajax/trainer_checkin.php',
              data: $form.serialize(),
              method: "POST",
              dataType: "json",
          }).always(function() {
              $taht.removeClass("disabled");
          }).success(function(data) {
              if (data.success) {
                window.location.reload();
              } else {
                  var $d = $('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <strong><i class="icon fa fa-ban"></i> Error</strong>&nbsp;<span></span></div>');
                  $d.find('span').text(data.message);
                  $("#message-container").append($d);
              }
          });
          return false;
      });
  });
</script>
<?php include 'base_foot.php'; ?>

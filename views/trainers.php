    <!-- Content Header (Page header) -->
<div class="box collapsed-box" id="trainers-box">
        <div class="box-header with-border">
          <h3 class="box-title">Trainers</h3>
		  
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">
			<table id="datatables2" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
							<th></th>
							<th>Username</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Hours Required</th>
							<th>Manager id</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($result2)) {
						?>
						<tr>
							<td></td>
							<td><?=$row['username']?></td>
							<td><?=$row['first_name']?></td>
							<td><?=$row['last_name']?></td>
							<td><?=$row['hours_required']?></td>
							<td><?=$row['manager_id']?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
  </div>
  <!-- /.box -->
  
  <script type="text/javascript" charset="utf-8">
	var editor2; // use a global for the submit and return data rendering in the examples
	 
	$(document).on("ready",function() {
		editor2 = new $.fn.dataTable.Editor( {
			ajax: "ajax/trainers.php",
			table: "#datatables2",
			fields: [
				{
					label: "Username:",
					name: "users.username"
				}, {
					label: "Password:",
					name: "users.password"
				}, {
					label: "First Name:",
					name: "trainers.first_name"
				}, {
					label: "Last Name:",
					name: "trainers.last_name"
				},
				{
					label:"Hours Required:",
					name:"trainers.hours_required"
				},
				{
					label: "Manager:",
					name: "trainers.manager_id",
					type: "select",
					placeholder: "Select a manager"
				}
			]
		} );
	 
	   
		$('#datatables2').on( 'click', 'tbody td:not(:first-child):not(:nth-child(2))', function (e) {
			editor2.inline(this, {
				onBlur: 'submit',
			});
		} );
	 
		$('#datatables2').DataTable( {
			dom: "Bfrtip",
			ajax: "ajax/trainers.php",
			columns: [
				{
					data: null,
					defaultContent: '',
					className: 'select-checkbox',
					orderable: false
				},
				{ data: "users.username" },
				{ data: "trainers.first_name" },
				{ data: "trainers.last_name" },
				{ data: "trainers.hours_required" },
				{data:"trainers.manager_id", render: {"display": function( data, type, row, meta )  {
					return row.managers.first_name +" "+row.managers.last_name;
				}}}
			],
			select: {
				style:    'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "create", editor: editor2 },
				{ extend: "edit",   editor: editor2 },
				{ extend: "remove", editor: editor2 }
			]
		} );
	} );
</script>
